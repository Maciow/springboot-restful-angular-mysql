import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'userStatus'
})
export class UserStatusPipePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value ? 'Active' : 'Inactive';
  }

}
