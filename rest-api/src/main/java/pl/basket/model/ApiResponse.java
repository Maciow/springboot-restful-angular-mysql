package pl.basket.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
public class ApiResponse<T> {

    private Integer status;
    private Set<String> messages;
    private T result;
}
