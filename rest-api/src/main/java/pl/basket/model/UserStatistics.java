package pl.basket.model;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class UserStatistics {
    private long plusGames;
    private long maybeGames;
    private long minusGames;
    private long seenGames;
}
