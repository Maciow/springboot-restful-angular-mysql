import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ApiReponse} from '../model/api-reponse';
import {Vote} from '../model/vote';

@Injectable({
  providedIn: 'root'
})
export class VoteService {

  private BASE_URL = 'http://localhost:8080';
  private VOTE_URL = `${this.BASE_URL}/games/vote`;

  constructor(private http: HttpClient) { }

  voteForGame(vote: Vote): Observable<ApiReponse> {
    return this.http.post<ApiReponse>(this.VOTE_URL, vote);
  }
}
