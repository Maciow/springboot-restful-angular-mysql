import {Vote} from './vote';
import {Place} from './place';

export interface Game {
  id?: number;
  place: Place;
  description: string;
  date: any;
  time: any;
  timestamp: any;
  userDTO: any;
  plus: number;
  maybe: number;
  minus: number;
  comments?: [Comment];
  votes: [Vote];
}
