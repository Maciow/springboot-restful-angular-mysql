package pl.basket.utils;

public interface EmailSender {
    void sendEmail(String to, String subject, String text);
}
