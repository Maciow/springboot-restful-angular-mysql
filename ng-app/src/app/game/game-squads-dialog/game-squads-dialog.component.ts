import {Component, Inject, OnInit} from '@angular/core';
import {GameService} from '../../service/game.service';
import {MAT_DIALOG_DATA} from '@angular/material';
import {GameSquads} from '../../model/game-squads';
import {HttpParams} from '@angular/common/http';
import {ApiReponse} from '../../model/api-reponse';
import {BehaviorSubject, Observable} from 'rxjs';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-game-squads-dialog',
  templateUrl: './game-squads-dialog.component.html',
  styleUrls: ['./game-squads-dialog.component.css']
})
export class GameSquadsDialogComponent implements OnInit {

  constructor(private gameService: GameService,
              @Inject(MAT_DIALOG_DATA) public data: any) { }
  private minTeamsNumber: number;
  private maxTeamsNumber: number;
  private errorMessageSubject$: BehaviorSubject<string>;
  private errorMessage$: Observable<string>;
  private teamsSubject$: BehaviorSubject<GameSquads>;
  private teams$: Observable<GameSquads>;
  private teamsNumber;
  private teamsNumberOptions: Array<number>;
  private defaultTeamsNumber: FormControl;

  ngOnInit() {
    this.errorMessageSubject$ = new BehaviorSubject<string>(null);
    this.errorMessage$ = this.errorMessageSubject$.asObservable();
    this.teamsSubject$ = new BehaviorSubject<GameSquads>(null);
    this.teams$ = this.teamsSubject$.asObservable();
    this.teamsNumber = 2;
    this.generateSquads(this.teamsNumber);
    this.minTeamsNumber = 2;
    this.maxTeamsNumber = 4;
    this.teamsNumberOptions = this.generateTeamsNumberOption(this.data.plusUsersLength);
    this.defaultTeamsNumber = new FormControl(2);
  }

  generateSquads(teamsNumber) {
    this.teamsNumber = teamsNumber;
    const params = new HttpParams()
      .set('teamsNumber', this.teamsNumber);
    this.gameService.getGameSquads(this.data.gameId, params).subscribe((data: ApiReponse) => {
      if (data.status < 300) {
        this.teamsSubject$.next(data.result);
        this.errorMessageSubject$.next(null);
      } else {
        this.errorMessageSubject$.next(data.messages.pop());
      }
    });
  }

  generateTeamsNumberOption(plusUsersLength: number): Array<number> {
    if (plusUsersLength > this.maxTeamsNumber) {
      return [2, 3, 4];
    } else {
      const options = [];
      for (let i = this.minTeamsNumber; i <= plusUsersLength; i++){
        options.push(i);
      }
      return options;
    }
  }

  onChange(teamsNumber) {
    this.generateSquads(teamsNumber);
  }

}
