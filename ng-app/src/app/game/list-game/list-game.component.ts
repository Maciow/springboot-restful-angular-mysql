import {Component, OnInit, ViewChild} from '@angular/core';
import {Game} from '../../model/game';
import {GameService} from '../../service/game.service';
import {HttpParams} from '@angular/common/http';
import {MatDialog, PageEvent} from '@angular/material';
import {Vote} from '../../model/vote';
import {VoteService} from '../../service/vote.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {AuthenticationService} from '../../service/authentication.service';
import {User} from '../../model/user';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Comment} from '../../model/comment';
import {ApiReponse} from '../../model/api-reponse';
import {RemoveGameDialogComponent} from '../../user/admin/remove-game-dialog/remove-game-dialog.component';
import {EditGameDialogComponent} from '../edit-game-dialog/edit-game-dialog.component';
import {NotificationService} from '../../service/notification.service';
import {GameStatisticsDialogComponent} from '../game-statistics-dialog/game-statistics-dialog.component';
import {GameSquadsDialogComponent} from '../game-squads-dialog/game-squads-dialog.component';

@Component({
  selector: 'app-list-game',
  templateUrl: './list-game.component.html',
  styleUrls: ['./list-game.component.css']
})
export class ListGameComponent implements OnInit {

  private games$: Observable<Game[]>;
  private gamesSubject$: BehaviorSubject<Game[]>;
  private page;
  private size;
  private totalPages;
  private totalElements;
  private pageSizeOption;
  private panelOpenState: boolean;
  private sendCommentForm: FormGroup;
  private currentUser: User;

  constructor(private gameService: GameService, private voteService: VoteService,
              private authService: AuthenticationService, private formBuilder: FormBuilder,
              private notificationService: NotificationService, public dialog: MatDialog) { }

  ngOnInit() {
    this.notificationService.checkUserNotifications();
    this.sendCommentForm = this.formBuilder.group({
      id: [null],
      text: [''],
      userDTO: [null],
      gameDTO: [null]
    });
    this.page = 1;
    this.size = 5;
    this.pageSizeOption = [5, 10, 15];
    this.panelOpenState = false;
    this.gamesSubject$ = new BehaviorSubject<Game[]>(null);
    this.loadGames(this.page, this.size);
    this.games$ = this.gamesSubject$.asObservable();
    this.currentUser = this.authService.currentUserValue;
  }

  private loadGames(page, size) {
    this.page = page;
    this.size = size;
    let params = new HttpParams()
      .set('page', this.page)
      .set('size', this.size);
    this.gameService.getGames(params).subscribe(data => {
      this.gamesSubject$.next(data['content']);
      this.totalPages = data['totalPages'];
      this.totalElements = data['totalElements'];
    });
  }

  onSubmit(game: Game) {
    this.sendCommentForm.controls['gameDTO'].setValue(game);
    this.gameService.sendComment(this.sendCommentForm.value)
      .subscribe(() => {
        this.loadGames(this.page, this.size);
        this.sendCommentForm.controls['text'].setValue('');
      });
  }

  voteForGame(voteType: string, gameId: number) {
   let vote = new Vote(voteType, gameId);
   this.voteService.voteForGame(vote).subscribe(() => {
     this.loadGames(this.page, this.size);
   });
  }

  onPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.size = event.pageSize;
    this.loadGames(this.page, this.size);
  }

  isVoted(game: Game, voteType: string) {
    let isVotedValue = false;
    game.votes.filter((vote: Vote) => vote.voteType === voteType)
      .forEach(data => {
      if (data.userDTO.id === this.authService.currentUserValue.id) {
        isVotedValue = true;
      }
    });
    return isVotedValue;
  }

  isAdmin(user: User): boolean {
    return this.authService.isAdmin(user);
  }

  isAllowToEditOrDelete(game: Game): boolean {
    let isAdmin: boolean = this.authService.isAdmin(this.currentUser);
    let isOwner: boolean = game.userDTO.id === this.currentUser.id;
    return isAdmin || isOwner;
  }

  checkStatistics(gameId: number) {
    this.dialog.open(GameStatisticsDialogComponent, {
      data: {gameId: gameId}
    });
  }

  generateSquads(game: Game) {
    const dialogRef = this.dialog.open(GameSquadsDialogComponent, {
      data: {gameId: game.id, plusUsersLength: game.plus}
    });
  }

  removeGame(game: Game) {
    const dialogRef = this.dialog.open(RemoveGameDialogComponent, {
      data: {game: game}
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.loadGames(this.page, this.size);
      }
    });
  }

  editGame(game: Game) {
    const dialogRef = this.dialog.open(EditGameDialogComponent, {
      data: {game: game}
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.loadGames(this.page, this.size);
      }
    });
  }

  countComments(game: Game) {
    if (game.comments) {
      if (game.comments.length > 0) {
        if (game.comments.length !== 1) {
          return game.comments.length + ' comments';
        } else {
          return '1 comment';
        }
      } else {
        return 'There are no comments yet';
      }
    } else {
      return 'There are no comments yet';
    }
  }

}
