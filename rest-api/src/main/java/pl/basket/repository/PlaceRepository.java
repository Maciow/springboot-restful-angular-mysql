package pl.basket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.basket.model.Place;

public interface PlaceRepository extends JpaRepository<Place, Long> {
}
