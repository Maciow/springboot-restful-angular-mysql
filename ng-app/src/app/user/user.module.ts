import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  MatButtonModule,
  MatCardModule, MatDialogModule, MatExpansionModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatProgressBarModule, MatSelectModule, MatSnackBarModule,
  MatSortModule,
  MatTableModule, MatTooltipModule
} from '@angular/material';
import { AdminComponent } from './admin/admin.component';
import { UserStatusPipePipe } from './admin/user-status-pipe.pipe';
import {FlexLayoutModule} from '@angular/flex-layout';
import {EditUserDialogComponent, EditUserSnackbarComponent} from './admin/edit-user-dialog/edit-user-dialog.component';
import { UserRolePipePipe } from './admin/user-role-pipe.pipe';
import {RemoveGameDialogComponent, RemoveGameSnackbarComponent} from './admin/remove-game-dialog/remove-game-dialog.component';
import { NotificationComponent } from './notification/notification.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import {ChartsModule, WavesModule} from 'angular-bootstrap-md';
import {
  EditUserDetailsDialogComponent,
  EditUserDetailsSnackbarComponent
} from './user-details/edit-user-details-dialog/edit-user-details-dialog.component';

@NgModule({
  declarations: [RegisterComponent, LoginComponent, AdminComponent, UserStatusPipePipe, EditUserDialogComponent, EditUserSnackbarComponent, UserRolePipePipe, RemoveGameDialogComponent, RemoveGameSnackbarComponent, NotificationComponent, UserDetailsComponent, EditUserDetailsDialogComponent, EditUserDetailsSnackbarComponent],
  imports: [
    ChartsModule,
    CommonModule,
    UserRoutingModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    // Material
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatExpansionModule,
    MatIconModule,
    MatInputModule,
    MatOptionModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatSelectModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTooltipModule,
    // MDB BOOTSTRAP
    WavesModule
  ],
  entryComponents: [EditUserDialogComponent, EditUserSnackbarComponent, RemoveGameDialogComponent, RemoveGameSnackbarComponent, EditUserDetailsDialogComponent, EditUserDetailsSnackbarComponent]
})
export class UserModule { }
