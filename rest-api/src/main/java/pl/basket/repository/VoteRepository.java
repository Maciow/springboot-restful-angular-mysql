package pl.basket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.basket.model.Game;
import pl.basket.model.User;
import pl.basket.model.Vote;

import java.util.List;
import java.util.Optional;

@Repository
public interface VoteRepository extends JpaRepository<Vote, Long> {
    Optional<Vote> findByGameAndUser(Game game, User user);
    List<Vote> findAllByUser(User user);
    @Query(value = "SELECT * FROM votes WHERE game_id=:game_id", nativeQuery = true)
    List<Vote> findAllByGameId(@Param("game_id") Long gameId);
}
