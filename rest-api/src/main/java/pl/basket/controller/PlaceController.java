package pl.basket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.basket.model.DTO.PlaceDTO;
import pl.basket.service.PlaceService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/places")
public class PlaceController {

    private PlaceService placeService;

    @Autowired
    public void setPlaceService(PlaceService placeService) {
        this.placeService = placeService;
    }

    @GetMapping
    public List<PlaceDTO> getPlaces() {
        return this.placeService.getAllPlaces();
    }
}
