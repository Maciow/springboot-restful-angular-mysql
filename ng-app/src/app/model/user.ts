import {UserDetails} from './user-details';
import {Notification} from './notification';
import {UserStatistics} from './user-statistics';

export interface User {
  id?: number;
  nickname: string;
  email: string;
  password: string;
  confirmPassword: string;
  isApproved?: boolean;
  activationCode?: string;
  roles?: [{role: string, description: string}];
  notifications: Notification[];
  userDetails: UserDetails;
  userStatistics: UserStatistics;
  imageUrl: string;
}
