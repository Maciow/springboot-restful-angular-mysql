package pl.basket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.basket.model.*;
import pl.basket.model.DTO.GameDTO;
import pl.basket.model.DTO.VoteDTO;
import pl.basket.service.GameService;
import pl.basket.service.UserService;
import pl.basket.service.VoteService;

import javax.validation.Valid;
import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping("/games")
public class GameController {

    private static final String ROLE_USER = "ROLE_USER";

    private GameService gameService;
    private UserService userService;
    private VoteService voteService;

    @Autowired
    public void setVoteService(VoteService voteService) {
        this.voteService = voteService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setGameService(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping
    @Secured({ROLE_USER})
    public Page<GameDTO> getGames(@RequestParam(defaultValue = "1") int page,
                                  @RequestParam(defaultValue = "5") int size){
        return this.gameService.getGames(page-1,size);
    }

    @GetMapping("/{gameId}")
    @Secured({ROLE_USER})
    public ResponseEntity<ApiResponse<GameDTO>> getGame(@PathVariable Long gameId) {
        ApiResponse<GameDTO> response = this.gameService.findById(gameId);
        return ResponseEntity.status(response.getStatus()).body(response);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @Secured({ROLE_USER})
    public ResponseEntity<ApiResponse<GameDTO>> saveGame(@Valid @RequestBody final GameDTO game,
                                                         BindingResult result,
                                                         Authentication authentication) {
        if (result.hasErrors()){
            Set<String> messages = result.getFieldErrors()
                    .stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toSet());
            return ResponseEntity.status(409).body(new ApiResponse<>(409,messages,game));
        } else {
            ApiResponse<GameDTO> response = this.gameService.saveGame(game, authentication);
            URI location = ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(response.getResult().getId())
                    .toUri();
            return ResponseEntity.created(location).body(response);
        }
    }

    @PostMapping("/vote")
    @Secured({ROLE_USER})
    public ResponseEntity<ApiResponse<VoteDTO>> voteForGame(@Valid @RequestBody final VoteDTO voteDTO,
                                               BindingResult result,
                                               Authentication authentication) {
        if (result.hasErrors()) {
            Set<String> messages = result.getFieldErrors()
                    .stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toSet());
            return ResponseEntity.status(409).body(new ApiResponse<>(409,messages,voteDTO));
        } else {
            Game game = this.gameService.findById(voteDTO);
            User user = this.userService.findByEmail(authentication.getName());
            ApiResponse<VoteDTO> response = this.voteService.addVote(game,user, VoteType.valueOf(voteDTO.getVoteType()));
            return ResponseEntity.ok(response);
        }
    }

    @DeleteMapping("/{gameId}")
    @Secured({ROLE_USER})
    public ResponseEntity<ApiResponse<GameDTO>> removeGame(@PathVariable Long gameId,
                                              Authentication authentication) {
        ApiResponse<GameDTO> response = this.gameService.removeGame(gameId, authentication);
        return ResponseEntity.status(response.getStatus()).body(response);
    }

    @PutMapping("/{gameId}")
    @Secured({ROLE_USER})
    public ResponseEntity<ApiResponse<GameDTO>> editGame(@PathVariable Long gameId,
                                                         @RequestBody final GameDTO gameDTO,
                                                         Authentication authentication) {
        ApiResponse<GameDTO> response = this.gameService.editGame(gameId, gameDTO, authentication);
        return ResponseEntity.status(response.getStatus()).body(response);
    }

    @GetMapping("/statistics/{gameId}")
    public ResponseEntity<ApiResponse<GameStatistics>> getStatistics(@PathVariable Long gameId) {
        ApiResponse<GameStatistics> response = this.gameService.getGameStatistics(gameId);
        return ResponseEntity.status(response.getStatus()).body(response);
    }

    @GetMapping("/{gameId}/game-squads")
    public ResponseEntity<ApiResponse<GameSquads>> getGameSquads(@PathVariable Long gameId,
                                                                 @RequestParam(defaultValue = "2") int teamsNumber) {
        ApiResponse<GameSquads> response = this.gameService.getGameSquads(gameId, teamsNumber);
        return ResponseEntity.status(response.getStatus()).body(response);
    }
}
