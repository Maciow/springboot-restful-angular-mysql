package pl.basket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.basket.mapper.Mapper;
import pl.basket.model.ApiResponse;
import pl.basket.model.DTO.UserDTO;
import pl.basket.model.User;
import pl.basket.service.UserService;

import javax.validation.Valid;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
public class UserController {

    private UserService userService;
    private Mapper<User, UserDTO> userMapper;

    @Autowired
    public void setUserMapper(Mapper<User, UserDTO> userMapper) {
        this.userMapper = userMapper;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public ResponseEntity<ApiResponse<String>> saveUser(@Valid @RequestBody final User user,
                                                      BindingResult result) {
        if (result.hasErrors()) {
            Set<String> messages = result.getFieldErrors()
                    .stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toSet());
            return ResponseEntity.status(409).body(new ApiResponse<>(409,messages,user.getEmail()));
        } else {
            return ResponseEntity.status(201).body(this.userService.saveUser(user));
        }
    }

    @GetMapping("/activatelink/{code}")
    public ResponseEntity<ApiResponse<String>> approveUser(@PathVariable final String code){
        ApiResponse<String> response = this.userService.approveUser(code);
        if (response.getStatus() == 200) {
            return ResponseEntity.ok(response);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/user/nickname/{nickname}")
    public boolean isNicknameExists(@PathVariable String nickname) {
        return this.userService.isNicknameExists(nickname);
    }

    @GetMapping("/user/email/{email}")
    public boolean isEmailExists(@PathVariable String email) {
        return this.userService.isEmailExists(email);
    }

    @GetMapping("/principals")
    public ResponseEntity<UserDTO> getUser(Authentication authentication) {
        User user = this.userService.findByEmail(authentication.getName());
        if (user != null) {
            return ResponseEntity.ok(this.userMapper.convertToDTO(user));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/users/{userId}")
    public ResponseEntity<ApiResponse<UserDTO>> getUserById(@PathVariable Long userId) {
        ApiResponse<UserDTO> response = this.userService.findByIdUserWithStats(userId);
        if (response.getStatus() < 300) {
            return ResponseEntity.status(response.getStatus()).body(response);
        } else {
            return ResponseEntity.status(response.getStatus()).body(response);
        }
    }
}
