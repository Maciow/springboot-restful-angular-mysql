package pl.basket.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import pl.basket.model.DTO.UserDTO;

import java.util.List;

@Getter
@Setter
@Builder
public class GameStatistics {
    private List<UserDTO> plusUsers;
    private List<UserDTO> maybeUsers;
    private List<UserDTO> minusUsers;
}
