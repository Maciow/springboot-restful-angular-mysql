import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Place} from '../../model/place';
import {AdminService} from '../../service/admin.service';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {Observable} from 'rxjs';
import {GameService} from '../../service/game.service';
import {map, startWith} from 'rxjs/operators';
import {SnackbarService} from '../../service/snackbar.service';
import {ApiReponse} from '../../model/api-reponse';

@Component({
  selector: 'app-edit-game-dialog',
  templateUrl: './edit-game-dialog.component.html',
  styleUrls: ['./edit-game-dialog.component.css']
})
export class EditGameDialogComponent implements OnInit {
  private editGameForm: FormGroup;
  private filteredOptions: Observable<Place[]>;
  private options: Place[];
  private minDate: Date;

  constructor(private formBuilder: FormBuilder,
              private gameService: GameService,
              private adminService: AdminService,
              private snackbarService: SnackbarService,
              private snackbar: MatSnackBar,
              private dialogRef: MatDialogRef<EditGameDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.editGameForm = this.formBuilder.group({
      place: [this.data.game.place, Validators.required],
      description: [this.data.game.description],
      date: [this.getDefaultDate(this.data.game.date), Validators.required],
      time: [this.data.game.time, Validators.required]
    });
    this.gameService.getPlaces().subscribe(data => {
      this.options = data;
      this.filteredOptions = this.editGameForm.controls['place'].valueChanges.pipe(
        startWith(''),
        map(value => value ? this.filter(value) : this.options.slice())
      );
    });
    this.minDate = new Date();
  }

  private filter(value: string): Place[] {
    const filterValue = (typeof value === 'string') ? value.toLocaleLowerCase() : this.getAddress(value);
    return this.options.filter(option => option.address.toLocaleLowerCase().includes(filterValue));
  }

  private getAddress(place: Place) {
    return place.address.toLocaleLowerCase();
  }

  private getDefaultDate(date: string): Date {
    const array: string[] = this.data.game.date.split('-');
    return new Date(Number(array[0]), Number(array[1]) - 1, Number(array[2]));
  }

  getPlaceAddress(place: object) {
    return place ? place['address'] : undefined;
  }

  onSubmit() {
    this.adminService.updateGame(this.editGameForm.value, this.data.game.id).subscribe((data: ApiReponse) => {
      this.snackbarService.messageSubject.next(data.messages.pop());
      this.openSnackbar();
    });
    this.dialogRef.close(true);
  }

  onNoClick() {
    this.dialogRef.close(false);
  }

  private openSnackbar() {
    this.snackbar.openFromComponent(EditGameSnackbarComponent, {
      duration: 5000
    });
  }

}

@Component({
  selector: 'app-edit-game-snackbar',
  template: '<span class="snackbar-text">{{message$ | async}}</span>',
  styles: [
    '.snackbar-text {color: gold}'
  ]
})
export class EditGameSnackbarComponent implements OnInit {
  message$: Observable<string>;

  constructor(private snackbarService: SnackbarService) {}

  ngOnInit(): void {
    this.message$ = this.snackbarService.message$;
  }

}
