package pl.basket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping("/oauth")
public class TokenController {

    private DefaultTokenServices tokenServices;
    private TokenStore tokenStore;

    @Autowired
    public void setTokenStore(TokenStore tokenStore) {
        this.tokenStore = tokenStore;
    }

    @Autowired
    public void setTokenServices(DefaultTokenServices tokenServices) {
        this.tokenServices = tokenServices;
    }

    @DeleteMapping("/revoke")
    @ResponseStatus(HttpStatus.OK)
    public void revokeToken(Authentication authentication){
        String userToken = ((OAuth2AuthenticationDetails) authentication.getDetails()).getTokenValue();
        tokenServices.revokeToken(userToken);
    }

    @GetMapping("/list")
    public List<String> findAllTokens() {
        Collection<OAuth2AccessToken> tokensByClientId = tokenStore.findTokensByClientId("my-client");
        return tokensByClientId.stream().map(OAuth2AccessToken::getValue).collect(Collectors.toList());
    }
}
