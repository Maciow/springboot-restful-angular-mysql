package pl.basket.model.DTO;

import lombok.*;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class PlaceDTO {

    private String id;
    @NotEmpty(message = "{pl.basket.model.DTO.PlaceDTO.address.NotEmpty}")
    private String address;
    private String imageUrl;
}
