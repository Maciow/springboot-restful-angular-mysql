package pl.basket.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.basket.model.Comment;
import pl.basket.model.DTO.CommentDTO;
import pl.basket.model.DTO.GameDTO;
import pl.basket.model.DTO.UserDTO;
import pl.basket.model.Game;
import pl.basket.model.User;

import java.sql.Timestamp;
import java.time.Instant;

@Component
public class CommentMapper implements Mapper<Comment, CommentDTO> {

    private Mapper<User, UserDTO> userMapper;
    private Mapper<Game, GameDTO> gameMapper;

    @Autowired
    public void setGameMapper(Mapper<Game, GameDTO> gameMapper) {
        this.gameMapper = gameMapper;
    }

    @Autowired
    public void setUserMapper(Mapper<User, UserDTO> userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public Comment convertToEntity(CommentDTO dto) {
        return Comment.builder()
                .id(dto.getId())
                .text(dto.getText())
                .user(userMapper.convertToEntity(dto.getUserDTO()))
                .game(gameMapper.convertToEntity(dto.getGameDTO()))
                .timestamp(Timestamp.from(Instant.now()))
                .build();
    }

    @Override
    public CommentDTO convertToDTO(Comment entity) {
        return CommentDTO.builder()
                .id(entity.getId())
                .text(entity.getText())
                .userDTO(userMapper.convertToDTO(entity.getUser()))
                .timestamp(entity.getTimestamp())
                .lastModified(entity.getLastModified())
                .build();
    }
}
