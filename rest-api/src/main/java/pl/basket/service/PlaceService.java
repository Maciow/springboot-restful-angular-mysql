package pl.basket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.basket.mapper.Mapper;
import pl.basket.model.DTO.PlaceDTO;
import pl.basket.model.Place;
import pl.basket.repository.PlaceRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlaceService {

    private PlaceRepository placeRepository;
    private Mapper<Place, PlaceDTO> mapper;

    @Autowired
    public void setMapper(Mapper<Place, PlaceDTO> mapper) {
        this.mapper = mapper;
    }

    @Autowired
    public void setPlaceRepository(PlaceRepository placeRepository) {
        this.placeRepository = placeRepository;
    }

    public List<PlaceDTO> getAllPlaces() {
        List<Place> places = this.placeRepository.findAll();
        return places.stream().map(mapper::convertToDTO).collect(Collectors.toList());
    }
}
