import {Component, OnInit} from '@angular/core';
import {User} from '../../model/user';
import {UserService} from '../../service/user.service';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {ApiReponse} from '../../model/api-reponse';
import {AuthenticationService} from '../../service/authentication.service';
import {UserDetails} from '../../model/user-details';
import {MatDialog} from '@angular/material';
import {EditUserDetailsDialogComponent} from './edit-user-details-dialog/edit-user-details-dialog.component';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  public chartType: string;
  public chartDatasets: Array<any>;
  public chartLabels: Array<string>;
  public chartColors: Array<any>;
  public chartOptions: any;

  public chartTypeBar: string;
  public chartDatasetsBar: Array<any>;
  public chartLabelsBar: Array<string>;
  public chartColorsBar: Array<any>;

  private user: User;
  private currentUser: User;

  constructor(private userService: UserService, private route: ActivatedRoute,
              private authService: AuthenticationService, public dialog: MatDialog) {
  }

  ngOnInit() {
    this.currentUser = this.authService.currentUserValue;
    this.chartType = 'radar';
    this.chartLabels = ['Jump range', 'Jump shot', 'Inside shot', 'Driving', 'Passing',
      'Shot blocking', 'Defencive', 'Rebounding', 'Handling', 'Stamina'];
    this.chartColors = [{
      backgroundColor: 'rgba(105, 0, 132, .2)',
      borderColor: 'rgba(200, 99, 132, .7)',
      borderWidth: 2,
    }];
    this.chartTypeBar = 'bar';
    this.chartLabelsBar = ['Plus', 'Maybe', 'Minus', 'Seen'];
    this.chartColorsBar = [{
      backgroundColor: [
        'rgba(75, 192, 192, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)'
      ],
      borderColor: [
        'rgba(75, 192, 192, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)'
      ],
      borderWidth: 2,
    }];
    this.chartOptions = {responsive: true};
    this.getUserData();
  }

  getUserData() {
    this.route.paramMap.pipe(switchMap((params: ParamMap) =>
      this.userService.getUser(params.get('id'))
    )).subscribe((data: ApiReponse) => {
      if (data.status < 300) {
        this.user = data.result;
        this.chartDatasets = [
          {
            data: [this.user.userDetails.jumpRange, this.user.userDetails.jumpShot,
              this.user.userDetails.insideShot, this.user.userDetails.driving,
              this.user.userDetails.passing, this.user.userDetails.shotBlocking,
              this.user.userDetails.defencive, this.user.userDetails.rebounding,
              this.user.userDetails.handling, this.user.userDetails.stamina],
            label: 'Skills'
          }
        ];
        this.chartDatasetsBar = [
          {
            data: [this.user.userStatistics.plusGames,
              this.user.userStatistics.maybeGames,
              this.user.userStatistics.minusGames,
              this.user.userStatistics.seenGames],
            label: 'Statistics'
          }
        ];
      }
    });
  }

  isAllowToEdit(userViewedId: number): boolean {
    const isAdmin: boolean = this.authService.isAdmin(this.currentUser);
    const isOwner: boolean = userViewedId === this.currentUser.id;
    return isOwner || isAdmin;
  }

  editUserDetails(user: User) {
    const dialogRef = this.dialog.open(EditUserDetailsDialogComponent,{
      data: {user: user}
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
       this.getUserData();
      }
    });
  }

  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }

}
