package pl.basket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.basket.mapper.Mapper;
import pl.basket.model.*;
import pl.basket.model.DTO.UserDTO;
import pl.basket.repository.UserRepository;
import pl.basket.repository.UserRoleRepository;
import pl.basket.utils.ActivationCodeUtils;
import pl.basket.utils.EmailSender;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final static String DEFAULT_ROLE = "ROLE_USER";
    private final static String USER_REGISTERED = "User has registered successfully";
    private final static String USER_APPROVED = "User has approved successfully";
    private final static String NO_SUCH_CODE = "There is no such activation code";
    private final static String DEFAULT_IMG_URL = "https://i.gyazo.com/ae1f5aef3908619b77f7d619352f9d19.png";
    private final static String USER_FOUNDED_SUCCESSFULLY = "User has been founded successfully";
    private final static String USER_NOT_FOUND = "There is no user with such id";

    private UserRepository userRepository;
    private EmailSender emailSender;
    private BCryptPasswordEncoder encoder;
    private UserRoleRepository userRoleRepository;
    private VoteService voteService;
    private Mapper<User, UserDTO> userMapper;

    @Autowired
    public void setVoteService(VoteService voteService) {
        this.voteService = voteService;
    }

    @Autowired
    public void setUserMapper(Mapper<User, UserDTO> userMapper) {
        this.userMapper = userMapper;
    }

    @Autowired
    public void setUserRoleRepository(UserRoleRepository userRoleRepository) {
        this.userRoleRepository = userRoleRepository;
    }

    @Autowired
    public void setEncoder(BCryptPasswordEncoder encoder) {
        this.encoder = encoder;
    }

    @Autowired
    public void setEmailSender(EmailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public ApiResponse<String> saveUser(final User user) {
        user.setPassword(encoder.encode(user.getPassword()));
        UserRole userRole = this.userRoleRepository.findByRole(DEFAULT_ROLE).orElse(null);
        user.getRoles().add(userRole);
        user.setActivationCode(ActivationCodeUtils.getActivationCode());
        user.setImageUrl(DEFAULT_IMG_URL);
        UserDetails userDetails = UserDetails.builder()
                .defencive(0).driving(0).handling(0)
                .height(180).insideShot(0).jumpRange(0)
                .jumpShot(0).passing(0).rebounding(0)
                .shotBlocking(0).stamina(0).weight(80)
                .overall(0)
                .build();
        user.setUserDetails(userDetails);
        this.userRepository.save(user);
        sendActivationLink(user);
        return new ApiResponse<>(201, Collections.singleton(USER_REGISTERED), user.getEmail());
    }

    public ApiResponse<UserDTO> findByIdUserWithStats(Long userId) {
        Optional<User> user = this.userRepository.findById(userId);
        if (user.isPresent()) {
            UserDTO dto = this.userMapper.convertToDTO(user.get());
            List<Vote> userVotes = this.voteService.findAllVotesByUser(user.get());
            UserStatistics userStatistics = UserStatistics.builder()
                    .plusGames(userVotes.stream().filter(v->v.getVoteType()==VoteType.PLUS).count())
                    .maybeGames(userVotes.stream().filter(v->v.getVoteType()==VoteType.MAYBE).count())
                    .minusGames(userVotes.stream().filter(v->v.getVoteType()==VoteType.MINUS).count())
                    .seenGames(0L)
                    .build();
            dto.setUserStatistics(userStatistics);
            return new ApiResponse<>(200, Collections.singleton(USER_FOUNDED_SUCCESSFULLY), dto);
        } else {
            return new ApiResponse<>(409, Collections.singleton(USER_NOT_FOUND), null);
        }
    }

    private void sendActivationLink(User user) {
        String text = "Open the link to confirm your account \n. http://localhost:8080/activatelink/"+user.getActivationCode();
        String subject = "Confirm your account";
        emailSender.sendEmail(user.getEmail(),subject,text);
    }

    public ApiResponse<String> approveUser(final String code) {
        User user = this.userRepository.findByActivationCode(code).orElse(null);
        if (user != null) {
            user.setIsApproved(true);
            this.userRepository.save(user);
            return new ApiResponse<>(200, Collections.singleton(USER_APPROVED), user.getEmail());
        } else {
            return new ApiResponse<>(404, Collections.singleton(NO_SUCH_CODE), code);
        }
    }

    public boolean isNicknameExists(String username) {
        return this.userRepository.findByNickname(username).isPresent();
    }

    public boolean isEmailExists(String email) {
        return this.userRepository.findByEmail(email).isPresent();
    }

    public User findByEmail(String email) {
        return this.userRepository.findByEmail(email).orElse(null);
    }
}
