import {User} from './user';

export interface GameStatistics {
  plusUsers: User[];
  maybeUsers: User[];
  minusUsers: User[];
}
