export interface ApiReponse {
  status: number;
  messages: Array<string>;
  result: any;
}
