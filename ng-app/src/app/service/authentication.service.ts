import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {Token} from '../model/token';
import {Router} from '@angular/router';
import {User} from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  public currentTokenSubject$: BehaviorSubject<Token>;
  public currentToken$: Observable<Token>;
  public currentUserSubject$: BehaviorSubject<User>;
  public currentUser$: Observable<User>;
  private loadingUserSubject$: BehaviorSubject<boolean>;
  public loadingUser$: Observable<boolean>;
  private errorLoginMessageSubject$: BehaviorSubject<string>;
  public errorLoginMessage$: Observable<string>;

  private BASE_URL = 'http://localhost:8080';
  private OAUTH2_TOKEN = `${this.BASE_URL}/oauth/token`;
  private REVOKE_TOKEN = `${this.BASE_URL}/oauth/revoke`;
  private USER_DETAILS = `${this.BASE_URL}/principals`;

  constructor(private http: HttpClient, private router: Router) {
    this.currentTokenSubject$ = new BehaviorSubject<Token>(JSON.parse(localStorage.getItem('token')));
    this.currentToken$ = this.currentTokenSubject$.asObservable();
    this.currentUserSubject$ = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser$')));
    this.currentUser$ = this.currentUserSubject$.asObservable();
    this.loadingUserSubject$ = new BehaviorSubject<boolean>(false);
    this.loadingUser$ = this.loadingUserSubject$.asObservable();
    this.errorLoginMessageSubject$ = new BehaviorSubject(null);
    this.errorLoginMessage$ = this.errorLoginMessageSubject$.asObservable();
  }

  public get currentTokenValue(): Token {
    return this.currentTokenSubject$.value;
  }

  public get currentUserValue(): User {
    return this.currentUserSubject$.value;
  }

  login(loginPayload) {
    this.loadingUserSubject$.next(true);
    const headers = {
      'Authorization': 'Basic ' + btoa('my-client:myclientsecret'),
      'Content-type': 'application/x-www-form-urlencoded'
    };
    return this.http.post<any>(this.OAUTH2_TOKEN, loginPayload, {headers})
      .subscribe(token => {
        if (token) {
          localStorage.removeItem('token');
          localStorage.setItem('token', JSON.stringify(token));
          this.currentTokenSubject$.next(token);
          this.http.get(this.USER_DETAILS).subscribe((data: User) => {
            this.currentUserSubject$.next(data);
            localStorage.setItem('currentUser$', JSON.stringify(data));
            this.loadingUserSubject$.next(false);
            this.router.navigate(['games']);
          });
        }
      }, err => {
        this.loadingUserSubject$.next(false);
        if (err.status === 400) {
          this.errorLoginMessageSubject$.next('Bad credentials');
        }
      });
  }

  isAdmin(user: User): boolean {
    let isAdminValue = false;
    user.roles.map(data => data.role).forEach(data => {
      if (data === 'ROLE_ADMIN') {
        isAdminValue = true;
      }
    });
    return isAdminValue;
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser$');
    this.http.delete(this.REVOKE_TOKEN);
    this.currentTokenSubject$.next(null);
    this.currentUserSubject$.next(null);
    this.loadingUserSubject$.next(false);
  }
}
