import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthenticationService} from '../service/authentication.service';
import {User} from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(private authService: AuthenticationService, private router: Router) {}

  canActivate():  boolean {
    let currentUser: User = this.authService.currentUserValue;
    if (!this.authService.isAdmin(currentUser)) {
      this.router.navigate(['games']);
      return false;
    }
    return true;
  }
}
