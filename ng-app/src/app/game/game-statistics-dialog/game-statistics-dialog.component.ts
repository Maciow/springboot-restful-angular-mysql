import {Component, Inject, OnInit} from '@angular/core';
import {GameService} from '../../service/game.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {GameStatistics} from '../../model/game-statistics';
import {ApiReponse} from '../../model/api-reponse';

@Component({
  selector: 'app-game-statistics-dialog',
  templateUrl: './game-statistics-dialog.component.html',
  styleUrls: ['./game-statistics-dialog.component.css']
})
export class GameStatisticsDialogComponent implements OnInit {

  private gameStatistics: GameStatistics;

  constructor(private gameService: GameService,
              public dialogRef: MatDialogRef<GameStatisticsDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.gameService.getGameStatistics(this.data.gameId).subscribe((data: ApiReponse) => {
      if (data.status < 300) {
        this.gameStatistics = data.result;
      }
    });
  }
}
