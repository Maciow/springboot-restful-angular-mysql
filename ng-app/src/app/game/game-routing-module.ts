import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListGameComponent} from './list-game/list-game.component';
import {AddGameComponent} from './add-game/add-game.component';
import {GameDetailComponent} from './game-detail/game-detail.component';

const gameRoutes: Routes = [
  {path: 'games', component: ListGameComponent},
  {path: 'games/add', component: AddGameComponent},
  {path: 'games/:id', component: GameDetailComponent}
];
@NgModule({
 exports: [
   RouterModule
 ],
  imports: [
    RouterModule.forChild(gameRoutes)
  ]
})
export class GameRoutingModule {
}
