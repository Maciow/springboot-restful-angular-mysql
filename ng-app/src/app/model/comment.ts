import {Game} from './game';
import {User} from './user';

export interface Comment {
  id?: number;
  text: string;
  userDTO?: User;
  gameDTO: Game;
}
