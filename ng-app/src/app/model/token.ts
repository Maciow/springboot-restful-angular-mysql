export interface Token {
  access_token: string;
  refresh_token: string;
  scope: string;
  token_type: string;
}
