import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GameService} from '../../service/game.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {Place} from '../../model/place';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-game',
  templateUrl: './add-game.component.html',
  styleUrls: ['./add-game.component.css']
})
export class AddGameComponent implements OnInit {

  addGameForm: FormGroup;
  options: Place[];
  filteredOptions: Observable<Place[]>;
  minDate: Date;
  private loadingBehavior$: BehaviorSubject<boolean>;
  private loadingStatus$: Observable<boolean>;

  constructor(private formBuilder: FormBuilder, private gameService: GameService,
              private router: Router) { }

  ngOnInit() {
    this.loadingBehavior$ = new BehaviorSubject<boolean>(false);
    this.loadingStatus$ = this.loadingBehavior$.asObservable();
    this.addGameForm = this.formBuilder.group({
      place: ['', Validators.required],
      description: [''],
      date: [null, Validators.required],
      time: [null, Validators.required]
    });
    this.gameService.getPlaces().subscribe(data => {
      this.options = data;
      this.filteredOptions = this.addGameForm.controls['place'].valueChanges.pipe(
        startWith(''),
        map(value => value ? this.filter(value) : this.options.slice())
      );
    });
    this.minDate = new Date();
  }

  private filter(value: string): Place[] {
    const filterValue = (typeof value === 'string') ? value.toLocaleLowerCase() : this.getAddress(value);
    return this.options.filter(option => option.address.toLocaleLowerCase().includes(filterValue));
  }

  private getAddress(place: Place) {
    return place.address.toLocaleLowerCase();
  }

  onSubmit() {
    this.loadingBehavior$.next(true);
    this.gameService.saveGame(this.addGameForm.value).subscribe(data => {
      this.router.navigate(['/games']);
      this.loadingBehavior$.next(false);
    });
  }

  getPlaceAddress(place: object) {
    return place ? place['address'] : undefined;
  }
}
