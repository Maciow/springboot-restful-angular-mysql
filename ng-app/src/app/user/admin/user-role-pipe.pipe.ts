import {Pipe, PipeTransform} from '@angular/core';
import {UserRole} from '../../model/user-role';

@Pipe({
  name: 'userRole'
})
export class UserRolePipePipe implements PipeTransform {

  transform(roles: UserRole[], args?: any): any {
    let roleNamesArray: Array<string> = [];
    for (let i = 0; i < roles.length; i++) {
      roleNamesArray.push(roles[i].role.substring(5));
    }
    return roleNamesArray.join(', ').toString();
  }

}
