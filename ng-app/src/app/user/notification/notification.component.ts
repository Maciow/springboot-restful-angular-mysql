import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {NotificationService} from '../../service/notification.service';
import {Notification} from '../../model/notification';
import {ApiReponse} from '../../model/api-reponse';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  private notifications$: Observable<Array<Notification>>;
  private itemToExpand: number;

  constructor(private notificationService: NotificationService) { }

  ngOnInit() {
    this.notificationService.checkUserNotifications();
    this.notifications$ = this.notificationService.notifications$;
    this.itemToExpand = 0;
  }

  markAsRead(notification: Notification) {
    if (!notification.read) {
      this.notificationService.changeStatusToRead(notification).subscribe((data: ApiReponse) => {
        if (data.result) {
          this.notificationService.checkUserNotifications();
          this.itemToExpand = notification.id;
        }
      });
    }
  }

}
