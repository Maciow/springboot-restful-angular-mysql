import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {AuthenticationService} from './authentication.service';
import {Notification} from '../model/notification';
import {ApiReponse} from '../model/api-reponse';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private BASE_URL = 'http://localhost:8080';
  private NOTIFICATIONS_URL = `${this.BASE_URL}/notifications/`;
  private NOTIFICATIONS_USER_URL = `${this.BASE_URL}/notifications/user/`;

  private notificationsSubject$: BehaviorSubject<Array<Notification>>;
  public notifications$: Observable<Array<Notification>>;
  private notificationsLengthSubject$: BehaviorSubject<number>;
  public notificationsLength$: Observable<number>;

  constructor(private http: HttpClient, private authService: AuthenticationService) {
    this.notificationsSubject$ = new BehaviorSubject<Array<Notification>>(null);
    this.notifications$ = this.notificationsSubject$.asObservable();
    this.notificationsLengthSubject$ = new BehaviorSubject<number>(0);
    this.notificationsLength$ = this.notificationsLengthSubject$.asObservable();
  }

  checkUserNotifications() {
    const currentUserId = this.authService.currentUserValue.id;
    return this.http.get(this.NOTIFICATIONS_USER_URL + currentUserId).subscribe((data: Array<Notification>) => {
      this.notificationsSubject$.next(data);
      const unreadNotifications = NotificationService.countUnreadNotifications(data);
      this.notificationsLengthSubject$.next(unreadNotifications);
    });
  }

  private static countUnreadNotifications(notifyArray: Array<Notification>): number {
    return notifyArray.filter(n => !n.read).length;
  }

  changeStatusToRead(notification: Notification): Observable<ApiReponse> {
    return this.http.put<ApiReponse>(this.NOTIFICATIONS_URL + notification.id, notification);
  }
}
