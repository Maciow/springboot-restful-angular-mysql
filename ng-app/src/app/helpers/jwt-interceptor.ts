import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AuthenticationService} from '../service/authentication.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private authService: AuthenticationService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let currentToken = this.authService.currentTokenValue;
    if (currentToken && currentToken.access_token) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${currentToken.access_token}`
        }
      });
    }
    return next.handle(req);
  }
}
