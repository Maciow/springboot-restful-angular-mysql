package pl.basket.service;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import pl.basket.model.ApiResponse;
import pl.basket.model.User;
import pl.basket.model.UserDetails;
import pl.basket.model.UserRole;
import pl.basket.repository.UserDetailsRepository;
import pl.basket.repository.UserRepository;
import pl.basket.repository.UserRoleRepository;

import java.util.Collections;
import java.util.Optional;

@Service
public class UserDetailsService {
    private static final String ROLE_ADMIN = "ROLE_ADMIN";
    private static final String USER_DETAILS_SAVED_SUCCESSFULLY = "User details has been saved successfully";
    private static final String USER_DETAILS_FAILURE = "User details cannot be saved";

    private UserRepository userRepository;
    private UserRoleRepository userRoleRepository;
    private UserDetailsRepository userDetailsRepository;

    public UserDetailsService(UserRepository userRepository, UserRoleRepository userRoleRepository, UserDetailsRepository userDetailsRepository) {
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;
        this.userDetailsRepository = userDetailsRepository;
    }

    public ApiResponse<UserDetails> saveUserDetails(UserDetails newDetails, Long userId, Authentication authentication) {
        Optional<User> currentUser = this.userRepository.findByEmail(authentication.getName());
        Optional<UserDetails> details = this.userDetailsRepository.findById(newDetails.getId());
        ApiResponse<UserDetails> responseSuccessful = new ApiResponse<>(201, Collections.singleton(USER_DETAILS_SAVED_SUCCESSFULLY), newDetails);
        if (currentUser.isPresent() && details.isPresent()) {
            int sumOfSkills = getSumOfSkillsFromDetails(newDetails);
            newDetails.setOverall(sumOfSkills);
            if (currentUser.get().getUserDetails().equals(newDetails)) {
                this.userDetailsRepository.save(newDetails);
                return responseSuccessful;
            } else {
                Optional<UserRole> roleAdmin = this.userRoleRepository.findByRole(ROLE_ADMIN);
                if (roleAdmin.isPresent() && currentUser.get().getRoles().contains(roleAdmin.get())) {
                    this.userDetailsRepository.save(newDetails);
                    return responseSuccessful;
                }
            }
        }
        return new ApiResponse<>(409, Collections.singleton(USER_DETAILS_FAILURE), newDetails);
    }

    private int getSumOfSkillsFromDetails(UserDetails userDetails) {
        int jumpRange = userDetails.getJumpRange();
        int jumpShot = userDetails.getJumpShot();
        int defencive = userDetails.getDefencive();
        int handling = userDetails.getHandling();
        int passing = userDetails.getPassing();
        int driving = userDetails.getDriving();
        int insideShot = userDetails.getInsideShot();
        int rebounding = userDetails.getRebounding();
        int shotBlocking = userDetails.getShotBlocking();
        int stamina = userDetails.getStamina();
        return jumpRange+jumpShot+defencive+handling+passing+driving+insideShot+rebounding+shotBlocking+stamina;
    }
}
