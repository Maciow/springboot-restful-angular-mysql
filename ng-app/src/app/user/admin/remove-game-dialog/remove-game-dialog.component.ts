import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {ApiReponse} from '../../../model/api-reponse';
import {AdminService} from '../../../service/admin.service';
import {SnackbarService} from '../../../service/snackbar.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-remove-game-dialog',
  templateUrl: './remove-game-dialog.component.html',
  styleUrls: ['./remove-game-dialog.component.css']
})
export class RemoveGameDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<RemoveGameDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private adminService: AdminService,
              private snackbar: MatSnackBar,
              private snackbarService: SnackbarService) { }

  ngOnInit() {
  }

  onSubmit() {
    this.adminService.deleteGame(this.data.game.id).subscribe((data: ApiReponse) => {
      this.snackbarService.messageSubject.next(data.messages.pop());
      this.openSnackbar();
      this.dialogRef.close(true);
    });
  }

  onNoClick() {
    this.dialogRef.close(false);
  }

  openSnackbar() {
    this.snackbar.openFromComponent(RemoveGameSnackbarComponent, {
      duration: 5000
    });
  }
}

@Component({
  selector: 'app-remove-game-snackbar',
  template: '<span class="snackbar-text">{{message$ | async}}</span>',
  styles: [
    '.snackbar-text {color: gold}'
  ]
})
export class RemoveGameSnackbarComponent implements OnInit{
  message$: Observable<string>;

  constructor(private snackbarService: SnackbarService) {}

  ngOnInit(): void {
    this.message$ = this.snackbarService.message$;
  }
}
