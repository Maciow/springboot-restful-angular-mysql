package pl.basket.model.DTO;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class VoteDTO {

    private Long id;
    @NotEmpty(message = "{pl.basket.model.DTO.VoteDTO.voteType.NotEmpty}")
    private String voteType;
    private UserDTO userDTO;
    @NotNull(message = "{pl.basket.model.DTO.VoteDTO.gameId.NotNull}")
    private Long gameId;
    private Timestamp timestamp;
    private Timestamp lastModified;
}
