package pl.basket.mapper;

import org.springframework.stereotype.Component;
import pl.basket.model.DTO.NotificationDTO;
import pl.basket.model.Notification;

@Component
class NotificationMapper implements Mapper<Notification, NotificationDTO> {
    @Override
    public Notification convertToEntity(NotificationDTO dto) {
        return Notification.builder()
                .id(dto.getId())
                .actionUrl(dto.getActionUrl())
                .isRead(dto.isRead())
                .title(dto.getTitle())
                .text(dto.getText())
                .timestamp(dto.getTimestamp())
                .build();
    }

    @Override
    public NotificationDTO convertToDTO(Notification entity) {
        return NotificationDTO.builder()
                .id(entity.getId())
                .actionUrl(entity.getActionUrl())
                .isRead(entity.isRead())
                .title(entity.getTitle())
                .text(entity.getText())
                .timestamp(entity.getTimestamp())
                .build();
    }
}
