package pl.basket.model.DTO;

import lombok.*;

import java.sql.Timestamp;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class NotificationDTO {
    private Long id;
    private String title;
    private String text;
    private boolean isRead;
    private String actionUrl;
    private Timestamp timestamp;
}
