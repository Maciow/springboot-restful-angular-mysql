package pl.basket.controller;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import pl.basket.model.ApiResponse;
import pl.basket.model.UserDetails;
import pl.basket.service.UserDetailsService;

import javax.validation.Valid;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping("/users")
public class UserDetailsController {
    private final static String ROLE_USER = "ROLE_USER";

    private UserDetailsService userDetailsService;

    public UserDetailsController(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @PutMapping("/{userId}/details")
    @Secured({ROLE_USER})
    public ResponseEntity<ApiResponse<UserDetails>> saveUserDetails(@Valid @RequestBody UserDetails userDetails,
                                                                BindingResult result,
                                                                @PathVariable Long userId,
                                                                Authentication authentication) {
        if (result.hasErrors()) {
            Set<String> messages = result.getFieldErrors().stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toSet());
            return ResponseEntity.status(409).body(new ApiResponse<>(409, messages, userDetails));
        } else {
            ApiResponse<UserDetails> response = this.userDetailsService.saveUserDetails(userDetails, userId, authentication);
            return ResponseEntity.status(response.getStatus()).body(response);
        }
    }
}
