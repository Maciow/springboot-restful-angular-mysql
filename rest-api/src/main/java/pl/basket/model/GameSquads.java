package pl.basket.model;

import lombok.Getter;
import lombok.Setter;
import pl.basket.model.DTO.UserDTO;

import java.util.List;

@Setter
@Getter
public class GameSquads {
    private List<List<UserDTO>> teams;
}
