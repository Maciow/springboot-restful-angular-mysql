import {Component, OnInit, ViewChild} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {User} from '../../model/user';
import {AdminService} from '../../service/admin.service';
import {HttpParams} from '@angular/common/http';
import {MatDialog, MatSort, MatTableDataSource, PageEvent} from '@angular/material';
import {EditUserDialogComponent} from './edit-user-dialog/edit-user-dialog.component';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;

  private users: MatTableDataSource<User>;
  private usersSubject$: BehaviorSubject<User[]>;
  private page;
  private size;
  private totalPages: number;
  private totalElements: number;
  private pageSizeOptions: number[];
  private displayedColumns: string[];

  constructor(private adminService: AdminService,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.page = 1;
    this.size = 10;
    this.pageSizeOptions = [5, 10, 15];
    this.usersSubject$ = new BehaviorSubject<User[]>(null);
    this.displayedColumns = ['id', 'nickname', 'email', 'isApproved', 'roles', 'avatar', 'edit'];
    this.loadUsers(this.page, this.size);
  }

  loadUsers(page, size) {
    this.page = page;
    this.size = size;
    const params = new HttpParams()
      .set('page', this.page)
      .set('size', this.size);
    this.adminService.getUsersList(params).subscribe(data => {
      this.usersSubject$.next(data['content']);
      this.users = new MatTableDataSource(this.usersSubject$.value);
      this.users.sort = this.sort;
      this.totalPages = data['totalPages'];
      this.totalElements = data['totalElements'];
    });
  }

  onPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.size = event.pageSize;
    this.loadUsers(this.page, this.size);
  }

  openDialog(user: User) {
    const dialogRef = this.dialog.open(EditUserDialogComponent, {
      data: {user: user}
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.loadUsers(this.page, this.size);
      }
    });
  }

}
