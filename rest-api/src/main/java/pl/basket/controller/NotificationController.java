package pl.basket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.basket.model.ApiResponse;
import pl.basket.model.DTO.NotificationDTO;
import pl.basket.service.NotificationService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/notifications")
class NotificationController {

    private final static String ROLE_USER = "ROLE_USER";

    private NotificationService notificationService;

    @Autowired
    public void setNotificationService(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping("/user/{userId}")
    @Secured({ROLE_USER})
    public List<NotificationDTO> getNotificationsFromUser(@PathVariable Long userId) {
        return this.notificationService.getNotificationFromUser(userId);
    }

    @PutMapping("/{notifyId}")
    @Secured({ROLE_USER})
    public ResponseEntity<ApiResponse<Boolean>> changeStatusToRead(@RequestBody NotificationDTO notificationDTO,
                                                          @PathVariable Long notifyId) {
        ApiResponse<Boolean> response = this.notificationService.markAsRead(notificationDTO, notifyId);
        if (response.getStatus() < 300) {
            return ResponseEntity.status(response.getStatus()).body(response);
        } else {
            return ResponseEntity.status(response.getStatus()).body(response);
        }
    }
}
