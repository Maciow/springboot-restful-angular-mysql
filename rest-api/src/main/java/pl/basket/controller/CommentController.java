package pl.basket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.basket.model.ApiResponse;
import pl.basket.model.DTO.CommentDTO;
import pl.basket.service.CommentService;

import javax.validation.Valid;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping("/comments")
public class CommentController {

    private static final String ROLE_USER = "ROLE_USER";

    private CommentService commentService;

    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping
    @Secured({ROLE_USER})
    public ResponseEntity<ApiResponse<CommentDTO>> saveComment(@Valid @RequestBody CommentDTO commentDTO,
                                                               BindingResult result,
                                                               Authentication authentication) {
        if (result.hasErrors()) {
            Set<String> messages = result.getFieldErrors()
                    .stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toSet());
            return ResponseEntity.status(409).body(new ApiResponse<>(409, messages, commentDTO));
        } else {
            ApiResponse<CommentDTO> response = this.commentService.saveComment(commentDTO, authentication);
            return ResponseEntity.ok(response);
        }
    }
}
