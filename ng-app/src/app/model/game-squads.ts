import {User} from './user';

export interface GameSquads {
  teams: Array<Array<User>>;
}
