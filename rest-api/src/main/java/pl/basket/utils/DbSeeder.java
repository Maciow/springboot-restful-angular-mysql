package pl.basket.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import pl.basket.repository.GameRepository;
import pl.basket.repository.NotificationRepository;

import java.time.LocalDateTime;

//@Service
class DbSeeder implements CommandLineRunner {
    private NotificationRepository notificationRepository;
    private GameRepository gameRepository;

    public DbSeeder(NotificationRepository notificationRepository, GameRepository gameRepository) {
        this.notificationRepository = notificationRepository;
        this.gameRepository = gameRepository;
    }

    @Override
    public void run(String... args) throws Exception {
//        LocalDateTime dateTime = LocalDateTime.now().plusHours(3);
//        notificationRepository.deleteAllByExpirationDateTimeBefore(dateTime);
        this.gameRepository.deleteAll();
    }
}
