export interface Place {
  id?: number;
  address: string;
  imageUrl: string;
}
