package pl.basket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import pl.basket.mapper.Mapper;
import pl.basket.model.*;
import pl.basket.model.DTO.GameDTO;
import pl.basket.model.DTO.UserDTO;
import pl.basket.model.DTO.VoteDTO;
import pl.basket.repository.GameRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class GameService {

    private final static String GAME_CREATED_SUCCESSFULLY = "Game proposal has been added successfully";
    private final static String GAME_REMOVED_SUCCESSFULLY = "Game from %s - %s has been removed successfully";
    private final static String GAME_FOUNDED_SUCCESSFULLY = "Game with id: %s has been founded successfully";
    private final static String GAME_NOT_FOUND = "There is no game with such id";
    private final static String NOT_ALLOWED_TO_REMOVE_GAME = "You are not allowed to remove the game";
    private final static String GAME_UPDATED_SUCCESSFULLY = "Game from %s - %s has been updated successfully";
    private final static String NOT_ALLOWED_TO_UPDATE_GAME = "You are not allowed to update the game";
    private final static String STATISTICS_FOUND = "PLUS: %s users, MAYBE: %s users, MINUS: %s users";
    private final static String STATISTICS_NOT_FOUND = "There are no statistics for game with %s ID";
    private final static String GAME_SQUADS_CREATED = "%s teams has been created";
    private final static String GAME_SQUADS_FAILURE = "Not enough users to make %s teams";
    private final static String ROLE_ADMIN = "ROLE_ADMIN";

    private GameRepository gameRepository;
    private Mapper<Game,GameDTO> gameMapper;
    private Mapper<User, UserDTO> userMapper;
    private UserService userService;
    private VoteService voteService;
    private NotificationService notificationService;

    @Autowired
    public void setUserMapper(Mapper<User, UserDTO> userMapper) {
        this.userMapper = userMapper;
    }

    @Autowired
    public void setNotificationService(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @Autowired
    public void setVoteService(VoteService voteService) {
        this.voteService = voteService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setGameMapper(Mapper<Game, GameDTO> gameMapper) {
        this.gameMapper = gameMapper;
    }

    @Autowired
    public void setGameRepository(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public Page<GameDTO> getGames(int page, int size){
        Page<Game> games = this.gameRepository.findAll(PageRequest.of(page,size));
        return games.map(game -> gameMapper.convertToDTO(game));
    }

    public ApiResponse<GameDTO> saveGame(final GameDTO gameDTO, Authentication authentication) {
        Game gameEntity = this.gameMapper.convertToEntity(gameDTO);
        User user = this.userService.findByEmail(authentication.getName());
        if (user != null) {
            gameEntity.setUser(user);
        }
        this.gameRepository.save(gameEntity);
        this.voteService.addVote(gameEntity, user, VoteType.PLUS);
        GameDTO convertedDTO = this.gameMapper.convertToDTO(gameEntity);
        this.notificationService.saveGameNotifications(convertedDTO.getUserDTO().getId(), convertedDTO, gameEntity);
        return new ApiResponse<>(201, Collections.singleton(GAME_CREATED_SUCCESSFULLY),convertedDTO);
    }

    public Game findById(VoteDTO voteDTO) {
        return this.gameRepository.findById(voteDTO.getGameId()).orElse(null);
    }

    public ApiResponse<GameDTO> findById(Long gameId) {
        Optional<Game> game = this.gameRepository.findById(gameId);
        if (game.isPresent()) {
            GameDTO gameDTO = this.gameMapper.convertToDTO(game.get());
            return new ApiResponse<>(200, Collections.singleton(String.format(GAME_FOUNDED_SUCCESSFULLY, gameId)), gameDTO);
        } else {
            return new ApiResponse<>(409, Collections.singleton(GAME_NOT_FOUND), null);
        }
    }

    public ApiResponse<GameDTO> removeGame(Long gameId, Authentication authentication) {
       Game game = this.gameRepository.findById(gameId).orElse(null);
       User user = this.userService.findByEmail(authentication.getName());
       if (isAdminOrOwner(user, game)) {
           this.gameRepository.deleteById(gameId);
           GameDTO gameDTO = this.gameMapper.convertToDTO(game);
           return new ApiResponse<>(200, Collections.singleton(String.format(GAME_REMOVED_SUCCESSFULLY, gameDTO.getTime(), gameDTO.getDate())), gameDTO);
       } else {
           return new ApiResponse<>(401, Collections.singleton(NOT_ALLOWED_TO_REMOVE_GAME), null);
       }
    }

    public ApiResponse<GameDTO> editGame(Long gameId, GameDTO gameDTO, Authentication authentication) {
        User user = this.userService.findByEmail(authentication.getName());
        Game game = this.gameRepository.findById(gameId).orElse(null);
        Game gameFromDto = this.gameMapper.convertToEntity(gameDTO);
        if (isAdminOrOwner(user, game)) {
            if (gameFromDto.getPlace() != null) {
                game.setPlace(gameFromDto.getPlace());
            }
            if (gameFromDto.getGameDate() != null) {
                game.setGameDate(gameFromDto.getGameDate());
            }
            game.setDescription(gameDTO.getDescription());
            this.gameRepository.save(game);
            return new ApiResponse<>(200, Collections.singleton(String.format(GAME_UPDATED_SUCCESSFULLY, gameDTO.getTime(), gameDTO.getDate())), gameDTO);
        } else {
            return new ApiResponse<>(409, Collections.singleton(NOT_ALLOWED_TO_UPDATE_GAME), null);
        }
    }

    private boolean isAdminOrOwner(User user, Game game) {
        if (user != null && game != null) {
            if (game.getUser().getId().equals(user.getId())) {
                return true;
            }
            for (UserRole role: user.getRoles()) {
                if (role.getRole().equals(ROLE_ADMIN)) {
                    return true;
                }
            }
        }
        return false;
    }

    public ApiResponse<GameStatistics> getGameStatistics(Long gameId) {
        Optional<Game> game = this.gameRepository.findById(gameId);
        if (game.isPresent()) {
            List<UserDTO> plusUsers = game.get().getVotes().stream().filter(v->v.getVoteType()==VoteType.PLUS).map(v->userMapper.convertToDTO(v.getUser())).collect(Collectors.toList());
            List<UserDTO> maybeUsers = game.get().getVotes().stream().filter(v->v.getVoteType()==VoteType.MAYBE).map(v->userMapper.convertToDTO(v.getUser())).collect(Collectors.toList());
            List<UserDTO> minusUsers = game.get().getVotes().stream().filter(v->v.getVoteType()==VoteType.MINUS).map(v->userMapper.convertToDTO(v.getUser())).collect(Collectors.toList());
            GameStatistics statistics = GameStatistics.builder()
                    .plusUsers(plusUsers)
                    .maybeUsers(maybeUsers)
                    .minusUsers(minusUsers)
                    .build();
            return new ApiResponse<>(200,Collections.singleton(String.format(STATISTICS_FOUND, plusUsers.size(), maybeUsers.size(), minusUsers.size())), statistics);
        } else {
            return new ApiResponse<>(409, Collections.singleton(String.format(STATISTICS_NOT_FOUND, gameId)), null);
        }
    }

    public ApiResponse<GameSquads> getGameSquads(Long gameId, int teamsNumber) {
        List<Vote> votes = this.voteService.findAllVotesByGameId(gameId);
        List<UserDTO> users = votes.stream().filter(v->v.getVoteType()==VoteType.PLUS).map(v -> userMapper.convertToDTO(v.getUser())).sorted(Comparator.comparing(u -> u.getUserDetails().getOverall())).collect(Collectors.toList());
        GameSquads squads = new GameSquads();
        if (users.size() >= teamsNumber) {
            squads.setTeams(getListOfEmptyLists(teamsNumber));
            for (int i=0; i<teamsNumber; i++) {
                squads.getTeams().get(i).add(users.get(i));
                for (int j=users.size()-i-1; j>teamsNumber-1; j-=teamsNumber) {
                    squads.getTeams().get(i).add(users.get(j));
                }
            }
            return new ApiResponse<>(200, Collections.singleton(String.format(GAME_SQUADS_CREATED, teamsNumber)), squads);
        } else {
            return new ApiResponse<>(409,Collections.singleton(String.format(GAME_SQUADS_FAILURE, teamsNumber)),null);
        }
    }

    private List<List<UserDTO>> getListOfEmptyLists(int teamsNumber) {
        List<List<UserDTO>> listOfLists = new ArrayList<>();
        if (teamsNumber > 0) {
            for (int i=1; i<=teamsNumber; i++) {
                listOfLists.add(new ArrayList<>());
            }
        }
        return listOfLists;
    }
}
