package pl.basket.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "users")
@Getter
@Setter
@ToString
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user")
    private Long id;
    @NotEmpty(message = "{pl.basket.model.User.nickname.NotEmpty}")
    @Size(min = 3, max = 15, message = "{pl.basket.model.User.nickname.Size}")
    @Column(unique = true)
    private String nickname;
    @NotEmpty(message = "{pl.basket.model.User.email.NotEmpty}")
    @Email(message = "{pl.basket.model.User.email.Email}")
    @Column(unique = true)
    private String email;
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id_user")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id_role")})
    private Set<UserRole> roles;
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "id_user")
    private Set<Notification> notifications;
    @NotEmpty(message = "{pl.basket.model.User.password.NotEmpty}")
    private String password;
    @Transient
    private String confirmPassword;
    @Column(name = "activation_code")
    private String activationCode;
    @Column(name = "is_approved")
    private Boolean isApproved;
    @Column(name = "image_url")
    private String imageUrl;
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "user_details_id")
    private UserDetails userDetails;

    public User() {
        this.roles = new HashSet<>();
        this.isApproved = false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
