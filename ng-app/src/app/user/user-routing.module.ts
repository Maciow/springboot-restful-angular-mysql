import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RegisterComponent} from './register/register.component';
import {LoginComponent} from './login/login.component';
import {AdminComponent} from './admin/admin.component';
import {AdminGuard} from '../guard/admin.guard';
import {NotificationComponent} from './notification/notification.component';
import {UserDetailsComponent} from './user-details/user-details.component';

const routes: Routes = [
  {path: 'register', component: RegisterComponent},
  {path: 'notifications', component: NotificationComponent},
  {path: 'login', component: LoginComponent},
  {path: 'admin', component: AdminComponent, canActivate: [AdminGuard]},
  {path: 'users/:id', component: UserDetailsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {
}
