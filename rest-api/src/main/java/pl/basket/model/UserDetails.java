package pl.basket.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "user_details")
@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class UserDetails implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user_details")
    private Long id;
    @Max(210)
    @Min(150)
    private int height;
    @Max(150)
    @Min(50)
    private int weight;
    @Max(5)
    @Min(0)
    @Column(name = "jump_range")
    private int jumpRange;
    @Max(5)
    @Min(0)
    @Column(name = "jump_shot")
    private int jumpShot;
    @Max(5)
    @Min(0)
    private int defencive;
    @Max(5)
    @Min(0)
    private int handling;
    @Max(5)
    @Min(0)
    private int passing;
    @Max(5)
    @Min(0)
    private int driving;
    @Max(5)
    @Min(0)
    @Column(name = "inside_shot")
    private int insideShot;
    @Max(5)
    @Min(0)
    private int rebounding;
    @Max(5)
    @Min(0)
    @Column(name = "shot_blocking")
    private int shotBlocking;
    @Max(5)
    @Min(0)
    private int stamina;
    private int overall;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDetails that = (UserDetails) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
