import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameStatisticsDialogComponent } from './game-statistics-dialog.component';

describe('GameStatisticsDialogComponent', () => {
  let component: GameStatisticsDialogComponent;
  let fixture: ComponentFixture<GameStatisticsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameStatisticsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameStatisticsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
