import {UserRole} from './user-role';

export class UserEditModel {
  nickname?: string;
  email?: string;
  isApproved?: boolean;
  roles?: UserRole[];
  imageUrl: string;

  constructor(nickname: string, email: string, isApproved: boolean, roles: UserRole[], imageUrl: string) {
    this.nickname = nickname;
    this.email = email;
    this.isApproved = isApproved;
    this.roles = roles;
    this.imageUrl = imageUrl;
  }
}
