package pl.basket.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.basket.model.*;
import pl.basket.model.DTO.*;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

@Component
public class GameMapper implements Mapper<Game, GameDTO> {

    private static final String DATE_SEPARATOR ="-";
    private static final String TIME_SEPARATOR =":";

    private Mapper<Place, PlaceDTO> placeMapper;
    private Mapper<User, UserDTO> userMapper;
    private Mapper<Vote, VoteDTO> voteMapper;
    private Mapper<Comment, CommentDTO> commentMapper;

    @Autowired
    public void setPlaceMapper(Mapper<Place, PlaceDTO> placeMapper) {
        this.placeMapper = placeMapper;
    }

    @Autowired
    public void setUserMapper(Mapper<User, UserDTO> userMapper) {
        this.userMapper = userMapper;
    }

    @Autowired
    public void setCommentMapper(Mapper<Comment, CommentDTO> commentMapper) {
        this.commentMapper = commentMapper;
    }

    @Autowired
    public void setVoteMapper(Mapper<Vote, VoteDTO> voteMapper) {
        this.voteMapper = voteMapper;
    }

    @Override
    public Game convertToEntity(GameDTO dto) {
        Long id = dto.getId() == null ? null : Long.valueOf(dto.getId());
        return Game.builder()
                .id(id)
                .place(placeMapper.convertToEntity(dto.getPlace()))
                .description(dto.getDescription())
                .gameDate(getLocalDateTimeFromStrings(dto.getDate(), dto.getTime()))
                .timestamp(Timestamp.from(Instant.now()))
                .comments(Collections.emptySet())
                .votes(Collections.emptySet())
                .build();
    }

    @Override
    public GameDTO convertToDTO(Game entity) {
        LocalDateTime gameDate = entity.getGameDate();
        return GameDTO.builder()
                .id(String.valueOf(entity.getId()))
                .userDTO(userMapper.convertToDTO(entity.getUser()))
                .place(placeMapper.convertToDTO(entity.getPlace()))
                .description(entity.getDescription())
                .date(getDateFromGame(gameDate))
                .time(getTimeFromGame(gameDate))
                .timestamp(entity.getTimestamp())
                .votes(entity.getVotes().stream().map(voteMapper::convertToDTO).collect(Collectors.toSet()))
                .comments(entity.getComments().stream().map(commentMapper::convertToDTO).sorted(Comparator.comparing(CommentDTO::getTimestamp)).collect(Collectors.toList()))
                .plus(entity.getVotes().stream().filter(x->x.getVoteType()== VoteType.PLUS).count())
                .maybe(entity.getVotes().stream().filter(x->x.getVoteType()== VoteType.MAYBE).count())
                .minus(entity.getVotes().stream().filter(x->x.getVoteType()== VoteType.MINUS).count())
                .build();
    }

    private String getDateFromGame(LocalDateTime time) {
        StringBuffer buffer = new StringBuffer();
        buffer.append(time.getYear());
        buffer.append(DATE_SEPARATOR);
        if (time.getMonth().getValue() < 10) {
            buffer.append("0");
        }
        buffer.append(time.getMonth().getValue());
        buffer.append(DATE_SEPARATOR);
        if (time.getDayOfMonth() < 10) {
            buffer.append("0");
        }
        buffer.append(time.getDayOfMonth());
        return buffer.toString();
    }

    private String getTimeFromGame(LocalDateTime time) {
        StringBuffer buffer = new StringBuffer();
        buffer.append(time.getHour());
        buffer.append(TIME_SEPARATOR);
        if (time.getMinute() < 10) {
            buffer.append("0");
        }
        buffer.append(time.getMinute());
        return buffer.toString();
    }

    private LocalDateTime getLocalDateTimeFromStrings(String date, String time) {
        String dateArray[] = date.split(DATE_SEPARATOR);
        String timeArray[] = time.split(TIME_SEPARATOR);
        int year = Integer.parseInt(dateArray[0]);
        int month = Integer.parseInt(dateArray[1]);
        int day = Integer.parseInt(dateArray[2].substring(0,2));
        int hour = Integer.parseInt(timeArray[0]);
        int minutes = Integer.parseInt(timeArray[1]);
        return LocalDateTime.of(year, month, day, hour, minutes);
    }

}
