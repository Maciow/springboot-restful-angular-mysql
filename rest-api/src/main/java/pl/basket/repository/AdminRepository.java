package pl.basket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.basket.model.User;

@Repository
public interface AdminRepository extends JpaRepository<User, Long> {
}
