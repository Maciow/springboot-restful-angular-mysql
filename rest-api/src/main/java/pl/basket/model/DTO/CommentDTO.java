package pl.basket.model.DTO;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class CommentDTO {

    private Long id;
    @NotEmpty(message = "{pl.basket.model.DTO.CommentDTO.text.NotEmpty}")
    private String text;
    private UserDTO userDTO;
    private GameDTO gameDTO;
    private Timestamp timestamp;
    private Timestamp lastModified;
}
