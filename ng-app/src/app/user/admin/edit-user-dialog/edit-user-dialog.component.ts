import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {AdminService} from '../../../service/admin.service';
import {SnackbarService} from '../../../service/snackbar.service';
import {Observable} from 'rxjs';
import {UserRole} from '../../../model/user-role';
import {UserEditModel} from '../../../model/user-edit-model';
import {ApiReponse} from '../../../model/api-reponse';

@Component({
  selector: 'app-edit-user-dialog',
  templateUrl: './edit-user-dialog.component.html',
  styleUrls: ['./edit-user-dialog.component.css']
})
export class EditUserDialogComponent implements OnInit {

  editForm: FormGroup;
  statusForm: FormControl;
  rolesForm: FormControl;
  allRoles: UserRole[];
  allStatuses: boolean[];

  constructor(private formBuilder: FormBuilder,
              private adminService: AdminService,
              private snackbarService: SnackbarService,
              private snackbar: MatSnackBar,
              public dialogRef: MatDialogRef<EditUserDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      nickname: [this.data.user.nickname, [Validators.required,
        Validators.minLength(3),
        Validators.maxLength(15)]],
      email: [this.data.user.email, [Validators.required,
        this.validateEmailPattern]],
      imageUrl: [this.data.user.imageUrl, [Validators.required]]
    });
    this.allRoles = [
      {role: 'ROLE_ADMIN'},
      {role: 'ROLE_USER'}];
    this.allStatuses = [true, false];
    this.statusForm = new FormControl();
    this.rolesForm = new FormControl(null, Validators.required);
    this.bindRoles();
    this.bindStatuses();
  }

  onSubmit() {
    let user = this.getUserFromForm();
    this.adminService.updateUser(user, this.data.user.id).subscribe((data: ApiReponse) => {
      this.snackbarService.messageSubject.next(data.messages.pop());
      this.openSnackbar();
    });
    this.dialogRef.close(true);
  }

  onNoClick() {
    this.dialogRef.close(false);
  }

  openSnackbar() {
    this.snackbar.openFromComponent(EditUserSnackbarComponent, {
      duration: 5000
    });
  }

  bindRoles() {
    const ownedRoles: UserRole[] = this.data.user.roles;
    const bindedRoles: UserRole[] = [];
    for (let i = 0; i < this.allRoles.length; i++) {
      for (let j = 0; j < ownedRoles.length; j++) {
        if (this.allRoles[i].role === ownedRoles[j].role) {
          bindedRoles.push(this.allRoles[i]);
        }
      }
    }
    this.rolesForm.setValue(bindedRoles);
  }

  bindStatuses() {
    let userStatus: boolean = this.data.user.isApproved;
    this.statusForm.setValue(userStatus);
  }

  getUserFromForm(): UserEditModel {
    let nickname: string = this.editForm.controls['nickname'].value;
    let email: string = this.editForm.controls['email'].value;
    let isApproved: boolean = this.statusForm.value;
    let imageUrl: string = this.editForm.controls['imageUrl'].value;
    let roles: UserRole[] = this.rolesForm.value;
    return new UserEditModel(nickname, email, isApproved, roles, imageUrl);
  }

  private validateEmailPattern(control: FormControl) {
    const EMAIL_REGEXP: RegExp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    return EMAIL_REGEXP.test(control.value) ? null : {
      validateEmailPattern: {
        valid: false
      }
    };
  }

}

@Component({
  selector: 'app-edit-user-snackbar',
  templateUrl: 'edit-user-snackbar.html',
  styles: [
    '.snackbar-text { color: gold }'
  ]
})
export class EditUserSnackbarComponent implements OnInit {

  message$: Observable<string>;

  constructor(private snackbarService: SnackbarService) {}

  ngOnInit() {
    this.message$ = this.snackbarService.message$;
  }
}
