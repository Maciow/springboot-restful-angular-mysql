import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../model/user';
import {ApiReponse} from '../model/api-reponse';
import {UserEditModel} from '../model/user-edit-model';
import {Game} from '../model/game';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private BASE_URL = 'http://localhost:8080';
  private USERS_URL = `${this.BASE_URL}/admin/users`;
  private GAME_URL = `${this.BASE_URL}/games/`;

  constructor(private http: HttpClient) { }

  getUsersList(params: HttpParams): Observable<any> {
    return this.http.get(this.USERS_URL, {params});
  }

  updateUser(user: UserEditModel, userId: number): Observable<ApiReponse> {
    return this.http.put<ApiReponse>(this.USERS_URL + '/' + userId, user);
  }

  deleteGame(gameId: number): Observable<ApiReponse> {
    return this.http.delete<ApiReponse>(this.GAME_URL + gameId);
  }

  updateGame(game: Game, gameId: number): Observable<ApiReponse> {
    return this.http.put<ApiReponse>(this.GAME_URL + gameId, game);
  }
}
