package pl.basket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.basket.model.ApiResponse;
import pl.basket.model.DTO.UserDTO;
import pl.basket.service.AdminService;

@RestController
@CrossOrigin
@RequestMapping("/admin")
public class AdminController {

    private final static String ROLE_ADMIN = "ROLE_ADMIN";

    private AdminService adminService;

    @Autowired
    public void setAdminService(AdminService adminService) {
        this.adminService = adminService;
    }

    @GetMapping("/users")
    @Secured({ROLE_ADMIN})
    public Page<UserDTO> getUsersList(@RequestParam(defaultValue = "1") int page,
                                      @RequestParam(defaultValue = "5") int size) {
        return this.adminService.getUsersList(page - 1, size);
    }

    @PutMapping("/users/{id}")
    @Secured({ROLE_ADMIN})
    public ResponseEntity<ApiResponse<UserDTO>> updateUser(@RequestBody UserDTO userDTO,
                                                           @PathVariable Long id) {
        ApiResponse<UserDTO> response = this.adminService.updateUser(id,userDTO);
        if (response.getStatus().equals(200)) {
            return ResponseEntity.status(200).body(response);
        } else {
            return ResponseEntity.status(409).body(response);
        }
    }
}
