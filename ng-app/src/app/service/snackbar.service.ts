import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  messageSubject: BehaviorSubject<string> = new BehaviorSubject(null);
  message$: Observable<string> = this.messageSubject.asObservable();

  constructor() { }
}
