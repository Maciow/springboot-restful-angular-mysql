package pl.basket.mapper;

import org.springframework.stereotype.Component;
import pl.basket.model.DTO.UserDTO;
import pl.basket.model.User;

@Component
public class UserMapper implements Mapper<User, UserDTO> {

    @Override
    public User convertToEntity(UserDTO dto) {
        return null;
    }

    @Override
    public UserDTO convertToDTO(User entity) {
        return UserDTO.builder()
                .email(entity.getEmail())
                .nickname(entity.getNickname())
                .id(entity.getId())
                .roles(entity.getRoles())
                .imageUrl(entity.getImageUrl())
                .userDetails(entity.getUserDetails())
                .isApproved(entity.getIsApproved()).build();
    }
}
