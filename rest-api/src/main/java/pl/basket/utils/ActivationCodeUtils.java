package pl.basket.utils;

import java.util.Random;

public class ActivationCodeUtils {

    public static String getActivationCode(){
        StringBuffer buffer = new StringBuffer();

        String signs = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

        Random random = new Random();

        for (int i=0; i<32; i++){
            int number = random.nextInt(signs.length());
            buffer.append(signs,number,number+1);
        }
        return buffer.toString();
    }
}
