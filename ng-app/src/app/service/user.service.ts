import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../model/user';
import {Observable} from 'rxjs';
import {ApiReponse} from '../model/api-reponse';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private BASE_URL = 'http://localhost:8080';
  private REGISTER_USER_URL = `${this.BASE_URL}/register`;
  private IS_NICKNAME_EXISTS_URL = `${this.BASE_URL}/user/nickname/`;
  private IS_EMAIL_EXISTS_URL = `${this.BASE_URL}/user/email/`;
  private USER_URL = `${this.BASE_URL}/users/`;

  constructor(private http: HttpClient) { }

  registerUser(user: User): Observable<ApiReponse> {
    return this.http.post<ApiReponse>(this.REGISTER_USER_URL, user);
  }

  isNicknameExists(nickname: string): Observable<boolean> {
    return this.http.get<boolean>(this.IS_NICKNAME_EXISTS_URL + nickname);
  }

  isEmailExists(email: string): Observable<boolean> {
    return this.http.get<boolean>(this.IS_EMAIL_EXISTS_URL + email);
  }

  getUser(userId: string): Observable<ApiReponse> {
    return this.http.get<ApiReponse>(this.USER_URL + userId);
  }
}
