export interface UserStatistics {
  plusGames: number;
  maybeGames: number;
  minusGames: number;
  seenGames: number;
}
