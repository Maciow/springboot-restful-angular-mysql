export interface Notification {
  id: number;
  title: string;
  text: string;
  read: boolean;
  actionUrl: string;
  timestamp: any;
}
