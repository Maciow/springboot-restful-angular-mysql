import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserDetails} from '../model/user-details';
import {Observable} from 'rxjs';
import {ApiReponse} from '../model/api-reponse';

@Injectable({
  providedIn: 'root'
})
export class UserDetailsService {
  private BASE_URL = 'http://localhost:8080';
  private USER_URL = `${this.BASE_URL}/users/`;

  constructor(private http: HttpClient) { }

  updateUserDetails(userDetails: UserDetails, userToEditId: number): Observable<ApiReponse> {
    return this.http.put<ApiReponse>(this.USER_URL + userToEditId + '/details', userDetails);
  }
}
