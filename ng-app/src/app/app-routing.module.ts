import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {GameModule} from './game/game.module';
import {UserModule} from './user/user.module';

const routes: Routes = [
  { path: '', redirectTo: '/games', pathMatch: 'full'}
];

@NgModule({
  exports: [
    RouterModule
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    GameModule,
    UserModule,
    RouterModule.forRoot(routes, {enableTracing: true})
  ]
})
export class AppRoutingModule {}
