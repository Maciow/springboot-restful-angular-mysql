import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../service/user.service';
import {ApiReponse} from '../../model/api-reponse';
import {Router} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  passwordForm: FormGroup;
  private loadingBehavior$: BehaviorSubject<boolean>;
  private loadingStatus$: Observable<boolean>;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private router: Router) { }

  ngOnInit() {
    this.loadingBehavior$ = new BehaviorSubject<boolean>(false);
    this.loadingStatus$ = this.loadingBehavior$.asObservable();
    this.registerForm = this.formBuilder.group({
      nickname: ['', [Validators.required,
        Validators.minLength(3),
        Validators.maxLength(15)],
        this.validateNicknameNotTaken.bind(this)
      ],
      email: ['', [Validators.required,
        this.validateEmailPattern],
      this.validateEmailNotTaken.bind(this)]
    });
    this.passwordForm = this.formBuilder.group({
      password: ['', [Validators.required,
        Validators.minLength(5),
        Validators.maxLength(20)]],
      confirmPassword: ['', [Validators.required]]
    }, {validator: this.validatePasswords.bind(this) });
  }

  onSubmit() {
    this.loadingBehavior$.next(true);
    let user = Object.assign(this.registerForm.value, this.passwordForm.value);
    this.userService.registerUser(user).subscribe((data: ApiReponse) => {
      if (data.status < 300) {
        this.router.navigate(['/login']);
      }
      this.loadingBehavior$.next(false);
    });
  }

  private validateNicknameNotTaken(control: FormControl) {
    let nickname: string = control.value;
    if (nickname.length > 4 && nickname.length < 16) {
      return this.userService.isNicknameExists(nickname).pipe(map(data => {
        return !data ? null : {nicknameTaken: true};
      }));
    }
    return Promise.resolve(null);
  }

  private validateEmailNotTaken(control: FormControl) {
    let email: string = control.value;
    if (email.includes('@')) {
      return this.userService.isEmailExists(email).pipe(map(data => {
        return !data ? null : {emailTaken: true};
      }));
    }
    return Promise.resolve(null);
  }

  private validateEmailPattern(control: FormControl) {
    const EMAIL_REGEXP: RegExp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    return EMAIL_REGEXP.test(control.value) ? null : {
      validateEmailPattern: {
        valid: false
      }
    };
  }

  private validatePasswords(group: FormGroup) {
    let password = group.controls.password.value;
    let confirmPassword = group.controls.confirmPassword.value;

    if (password.length > 0 && password !== confirmPassword) {
      group.controls.confirmPassword.setErrors({notMatch: true});
    } else {
      group.controls.confirmPassword.setErrors(null);
    }
  }
}
