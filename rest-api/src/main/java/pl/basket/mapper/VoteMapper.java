package pl.basket.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.basket.model.DTO.UserDTO;
import pl.basket.model.DTO.VoteDTO;
import pl.basket.model.User;
import pl.basket.model.Vote;
import pl.basket.model.VoteType;

@Component
public class VoteMapper implements Mapper<Vote, VoteDTO> {

    private Mapper<User, UserDTO> userMapper;

    @Autowired
    public void setUserMapper(Mapper<User, UserDTO> userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public Vote convertToEntity(VoteDTO dto) {
        return null;
    }

    @Override
    public VoteDTO convertToDTO(Vote entity) {
        return VoteDTO.builder()
                .id(entity.getId())
                .userDTO(userMapper.convertToDTO(entity.getUser()))
                .voteType(entity.getVoteType().name())
                .gameId(entity.getGame().getId())
                .timestamp(entity.getTimestamp())
                .lastModified(entity.getLastModified())
                .build();

    }
}
