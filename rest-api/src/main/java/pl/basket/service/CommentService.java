package pl.basket.service;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import pl.basket.mapper.Mapper;
import pl.basket.model.ApiResponse;
import pl.basket.model.Comment;
import pl.basket.model.DTO.CommentDTO;
import pl.basket.model.DTO.UserDTO;
import pl.basket.model.User;
import pl.basket.repository.CommentRepository;
import pl.basket.repository.UserRepository;

import java.util.Collections;

@Service
public class CommentService {

    private final static String SUCCESSFULL_ADD_COMMENT = "Comment has added successfully";

    private CommentRepository commentRepository;
    private UserRepository userRepository;
    private Mapper<User, UserDTO> userMapper;
    private Mapper<Comment, CommentDTO> commentMapper;

    @Autowired
    public void setCommentMapper(Mapper<Comment, CommentDTO> commentMapper) {
        this.commentMapper = commentMapper;
    }

    @Autowired
    public void setUserMapper(Mapper<User, UserDTO> userMapper) {
        this.userMapper = userMapper;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setCommentRepository(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    public ApiResponse<CommentDTO> saveComment(CommentDTO commentDTO, Authentication authentication) {
        User user = this.userRepository.findByEmail(authentication.getName()).orElse(null);
        Comment comment = commentMapper.convertToEntity(commentDTO);
        comment.setUser(user);
        this.commentRepository.save(comment);
        commentDTO.setUserDTO(userMapper.convertToDTO(user));
        return new ApiResponse<>(200, Collections.singleton(SUCCESSFULL_ADD_COMMENT), commentDTO);
    }
}
