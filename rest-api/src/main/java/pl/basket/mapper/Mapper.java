package pl.basket.mapper;

import org.springframework.stereotype.Component;
import pl.basket.model.DTO.GameDTO;
import pl.basket.model.DTO.PlaceDTO;
import pl.basket.model.DTO.UserDTO;
import pl.basket.model.Game;
import pl.basket.model.Place;
import pl.basket.model.User;

import java.time.LocalDateTime;

public interface Mapper<T,F> {
    T convertToEntity(F dto);
    F convertToDTO(T entity);
}
