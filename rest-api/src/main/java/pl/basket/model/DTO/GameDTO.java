package pl.basket.model.DTO;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@ToString
public class GameDTO {

    private String id;

    private PlaceDTO place;
    private String description;
    @NotEmpty(message = "{pl.basket.model.DTO.GameDTO.date.NotEmpty}")
    private String date;
    @NotEmpty(message = "{pl.basket.model.DTO.GameDTO.time.NotEmpty}")
    private String time;
    private Timestamp timestamp;
    private UserDTO userDTO;
    private List<CommentDTO> comments;
    private Set<VoteDTO> votes;
    private Long plus;
    private Long maybe;
    private Long minus;
}
