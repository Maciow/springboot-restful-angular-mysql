import {AfterViewInit, Component, OnInit} from '@angular/core';
import {User} from './model/user';
import {AuthenticationService} from './service/authentication.service';
import {Observable} from 'rxjs';
import {NotificationService} from './service/notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  private currentUser$: Observable<User>;
  private notificationsLength$: Observable<number>;

  constructor(private authService: AuthenticationService, private notificationService: NotificationService) {}

  ngOnInit(): void {
    this.currentUser$ = this.authService.currentUser$;
    this.notificationsLength$ = this.notificationService.notificationsLength$;
  }
}
