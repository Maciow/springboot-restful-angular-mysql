package pl.basket.mapper;

import org.springframework.stereotype.Component;
import pl.basket.model.DTO.PlaceDTO;
import pl.basket.model.Place;

@Component
public class PlaceMapper implements Mapper<Place, PlaceDTO> {

    @Override
    public Place convertToEntity(PlaceDTO dto) {
        Long id = dto.getId() == null ? null : Long.valueOf(dto.getId());
        return Place.builder()
                .id(id)
                .address(dto.getAddress())
                .imageUrl(dto.getImageUrl())
                .build();
    }

    @Override
    public PlaceDTO convertToDTO(Place entity) {
        return PlaceDTO.builder()
                .id(String.valueOf(entity.getId()))
                .address(entity.getAddress())
                .imageUrl(entity.getImageUrl())
                .build();
    }
}
