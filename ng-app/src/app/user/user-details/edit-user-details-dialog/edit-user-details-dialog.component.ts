import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {SnackbarService} from '../../../service/snackbar.service';
import {Observable} from 'rxjs';
import {UserDetailsService} from '../../../service/user-details.service';
import {User} from '../../../model/user';
import {ApiReponse} from '../../../model/api-reponse';

@Component({
  selector: 'app-edit-user-details-dialog',
  templateUrl: './edit-user-details-dialog.component.html',
  styleUrls: ['./edit-user-details-dialog.component.css']
})
export class EditUserDetailsDialogComponent implements OnInit {
  editUserDetailsForm: FormGroup;
  rates: Array<number>;

  constructor(private formBuilder: FormBuilder,
              public dialogRef: MatDialogRef<EditUserDetailsDialogComponent>,
              private userDetailsService: UserDetailsService,
              private snackbarService: SnackbarService,
              private snackbar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.rates = [0, 1, 2, 3, 4, 5];
    this.editUserDetailsForm = this.formBuilder.group({
      id: [this.data.user.userDetails.id],
      height: [this.data.user.userDetails.height, [Validators.required,
        Validators.min(150),
        Validators.max(210)]],
      weight: [this.data.user.userDetails.weight, [Validators.required,
        Validators.min(50),
        Validators.max(150)]],
      jumpRange: [this.data.user.userDetails.jumpRange],
      jumpShot: [this.data.user.userDetails.jumpShot],
      defencive: [this.data.user.userDetails.defencive],
      handling: [this.data.user.userDetails.handling],
      passing: [this.data.user.userDetails.passing],
      driving: [this.data.user.userDetails.driving],
      insideShot: [this.data.user.userDetails.insideShot],
      rebounding: [this.data.user.userDetails.rebounding],
      shotBlocking: [this.data.user.userDetails.shotBlocking],
      stamina: [this.data.user.userDetails.stamina]
    });
  }

  onSubmit() {
    this.userDetailsService.updateUserDetails(this.editUserDetailsForm.value, this.data.user.id).subscribe((data: ApiReponse) => {
      this.snackbarService.messageSubject.next(data.messages.pop());
      this.openSnackbar();
    });
    this.dialogRef.close(true);
  }

  onNoClick() {
    this.dialogRef.close(false);
  }

  openSnackbar() {
    this.snackbar.openFromComponent(EditUserDetailsSnackbarComponent, {
      duration: 5000
    });
  }

}

@Component({
  selector: 'app-edit-user-details-snackbar',
  template: '<span class="snackbar-text">{{message$ | async}}</span>',
  styles: [
    '.snackbar-text {color: gold}'
  ]
})
export class EditUserDetailsSnackbarComponent implements OnInit {
  private message$: Observable<string>;

  constructor(private snackbarService: SnackbarService) {
  }

  ngOnInit(): void {
    this.message$ = this.snackbarService.message$;
  }
}
