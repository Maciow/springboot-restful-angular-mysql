export interface UserDetails {
  id: number;
  height: number;
  weight: number;
  jumpRange: number;
  jumpShot: number;
  defencive: number;
  handling: number;
  passing: number;
  driving: number;
  insideShot: number;
  rebounding: number;
  shotBlocking: number;
  stamina: number;
}
