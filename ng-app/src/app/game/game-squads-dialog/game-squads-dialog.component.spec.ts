import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameSquadsDialogComponent } from './game-squads-dialog.component';

describe('GameSquadsDialogComponent', () => {
  let component: GameSquadsDialogComponent;
  let fixture: ComponentFixture<GameSquadsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameSquadsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameSquadsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
