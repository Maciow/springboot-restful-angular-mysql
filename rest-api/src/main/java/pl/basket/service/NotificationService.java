package pl.basket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.basket.mapper.Mapper;
import pl.basket.model.ApiResponse;
import pl.basket.model.DTO.GameDTO;
import pl.basket.model.DTO.NotificationDTO;
import pl.basket.model.Game;
import pl.basket.model.Notification;
import pl.basket.model.User;
import pl.basket.repository.NotificationRepository;
import pl.basket.repository.UserRepository;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class NotificationService {
    private static final String GAME_URL = "http://localhost:4200/games/%s";
    private static final String GAME_NOTIFICATION_TITLE = "%s - %s at %s";
    private static final String GAME_NOTIFICATION_TEXT = "%s invites you for a game: %s - %s at %s";
    private static final String NOTIFICATION_READ = "Notification has been marked as read";
    private static final String NOTIFICATION_ALREADY_READ = "Notification is already marked as read";

    private NotificationRepository notificationRepository;
    private UserRepository userRepository;
    private Mapper<Notification, NotificationDTO> notificationMapper;

    @Autowired
    public void setNotificationMapper(Mapper<Notification, NotificationDTO> notificationMapper) {
        this.notificationMapper = notificationMapper;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setNotificationRepository(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    void saveGameNotifications(Long gameOwnerId, GameDTO gameDTO, Game game) {
        if (gameDTO != null && gameOwnerId != null) {
            List<Long> userIds = this.userRepository.findAllUsersIds();
            userIds.remove(gameOwnerId);
            for (Long id : userIds) {
                Notification notification = Notification.builder()
                        .isRead(false)
                        .actionUrl(String.format(GAME_URL, gameDTO.getId()))
                        .title(String.format(GAME_NOTIFICATION_TITLE, gameDTO.getTime(), gameDTO.getDate(), gameDTO.getPlace().getAddress()))
                        .text(String.format(GAME_NOTIFICATION_TEXT, gameDTO.getUserDTO().getNickname(), gameDTO.getTime(), gameDTO.getDate(), gameDTO.getPlace().getAddress()))
                        .timestamp(Timestamp.from(Instant.now()))
                        .expirationDateTime(game.getGameDate())
                        .game(game)
                        .build();
                this.notificationRepository.save(notification);
                this.notificationRepository.setUserToGameNotification(id, notification.getId());
            }
        }
    }

    public List<NotificationDTO> getNotificationFromUser(Long userId) {
        Optional<User> user = this.userRepository.findById(userId);
        if (user.isPresent()) {
            List<NotificationDTO> notificationsDTO = new ArrayList<>();
            Set<Notification> notifications = user.get().getNotifications();
            for (Notification n : notifications) {
                NotificationDTO dto = this.notificationMapper.convertToDTO(n);
                notificationsDTO.add(dto);
            }
            return notificationsDTO;
        } else {
            return Collections.emptyList();
        }
    }

    public ApiResponse<Boolean> markAsRead(NotificationDTO notificationDTO, Long notifyId) {
        Optional<Notification> notification = this.notificationRepository.findById(notifyId);
        if (notification.isPresent()) {
            notification.get().setRead(true);
            this.notificationRepository.save(notification.get());
            return new ApiResponse<>(200, Collections.singleton(NOTIFICATION_READ), Boolean.TRUE);
        } else {
            return new ApiResponse<>(409, Collections.singleton(NOTIFICATION_ALREADY_READ), Boolean.FALSE);
        }
    }
}
