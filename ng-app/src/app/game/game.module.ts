import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListGameComponent } from './list-game/list-game.component';
import { AddGameComponent } from './add-game/add-game.component';
import {GameRoutingModule} from './game-routing-module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { GameDetailComponent } from './game-detail/game-detail.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule, MatExpansionModule,
  MatIconModule,
  MatInputModule, MatMenuModule, MatPaginatorModule, MatProgressBarModule, MatSelectModule, MatTooltipModule
} from '@angular/material';
import {MAT_MOMENT_DATE_FORMATS, MatMomentDateModule} from '@angular/material-moment-adapter';
import {MomentUtcDateAdaoter} from '../settings/moment-utc-date-adaoter';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {EditGameDialogComponent, EditGameSnackbarComponent} from './edit-game-dialog/edit-game-dialog.component';
import {EditUserSnackbarComponent} from '../user/admin/edit-user-dialog/edit-user-dialog.component';
import { GameStatisticsDialogComponent } from './game-statistics-dialog/game-statistics-dialog.component';
import {ScrollingModule} from '@angular/cdk/scrolling';
import { GameSquadsDialogComponent } from './game-squads-dialog/game-squads-dialog.component';

@NgModule({
  declarations: [ListGameComponent, AddGameComponent, GameDetailComponent, EditGameDialogComponent, EditGameSnackbarComponent, GameStatisticsDialogComponent, GameSquadsDialogComponent],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    GameRoutingModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    // Material
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatMomentDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatSelectModule,
    MatTooltipModule,
    NgxMaterialTimepickerModule.forRoot(),
    // Other
    ScrollingModule
  ],
  entryComponents: [EditGameDialogComponent, EditGameSnackbarComponent, GameSquadsDialogComponent, GameStatisticsDialogComponent],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    { provide: DateAdapter, useClass: MomentUtcDateAdaoter },
  ]
})
export class GameModule { }
