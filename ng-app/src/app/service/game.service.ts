import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Game} from '../model/game';
import {Observable} from 'rxjs';
import {ApiReponse} from '../model/api-reponse';
import {Comment} from '../model/comment';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private BASE_URL = 'http://localhost:8080';
  private GAMES_URL = `${this.BASE_URL}/games`;
  private STATISTICS_URL = `${this.GAMES_URL}/statistics/`;
  private PLACES_URL = `${this.BASE_URL}/places`;
  private COMMENT_URL = `${this.BASE_URL}/comments`;

  constructor(private http: HttpClient) { }

  saveGame(game: Game): Observable<ApiReponse> {
    return this.http.post<ApiReponse>(this.GAMES_URL, game);
  }

  getGames(params: HttpParams): Observable<any> {
    return this.http.get(this.GAMES_URL, {params});
  }

  getGame(gameId: string): Observable<ApiReponse> {
    return this.http.get<ApiReponse>(this.GAMES_URL + '/' + gameId);
  }

  getPlaces(): Observable<any> {
    return this.http.get(this.PLACES_URL);
  }

  sendComment(comment: Comment): Observable<ApiReponse> {
    return this.http.post<ApiReponse>(this.COMMENT_URL, comment);
  }

  getGameStatistics(gameId: number): Observable<ApiReponse> {
    return this.http.get<ApiReponse>(this.STATISTICS_URL + gameId);
  }

  getGameSquads(gameId: number, params: HttpParams): Observable<ApiReponse> {
    return this.http.get<ApiReponse>(this.GAMES_URL + '/' + gameId + '/game-squads', {params});
  }
}
