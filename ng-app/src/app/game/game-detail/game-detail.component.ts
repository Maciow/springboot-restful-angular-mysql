import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {GameService} from '../../service/game.service';
import {Game} from '../../model/game';
import {BehaviorSubject, Observable} from 'rxjs';
import {ApiReponse} from '../../model/api-reponse';
import {User} from '../../model/user';
import {AuthenticationService} from '../../service/authentication.service';
import {Vote} from '../../model/vote';
import {VoteService} from '../../service/vote.service';
import {MatDialog} from '@angular/material';
import {GameStatisticsDialogComponent} from '../game-statistics-dialog/game-statistics-dialog.component';
import {GameSquadsDialogComponent} from '../game-squads-dialog/game-squads-dialog.component';
import {EditGameDialogComponent} from '../edit-game-dialog/edit-game-dialog.component';
import {RemoveGameDialogComponent} from '../../user/admin/remove-game-dialog/remove-game-dialog.component';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.css']
})
export class GameDetailComponent implements OnInit {
  private gameId: string;
  private gameSubject$: BehaviorSubject<Game>;
  private game$: Observable<Game>;
  private currentUser: User;
  private panelOpenState: boolean;
  private sendCommentForm: FormGroup;

  constructor(private route: ActivatedRoute, private gameService: GameService,
              private authService: AuthenticationService, private voteService: VoteService,
              private formBuilder: FormBuilder, public dialog: MatDialog) { }

  ngOnInit() {
    this.sendCommentForm = this.formBuilder.group({
      id: [null],
      text: [''],
      userDTO: [null],
      gameDTO: [null]
    });
    this.gameId = this.route.snapshot.paramMap.get('id');
    this.gameSubject$ = new BehaviorSubject(null);
    this.game$ = this.gameSubject$.asObservable();
    this.getGame(this.gameId);
    this.currentUser = this.authService.currentUserValue;
    this.panelOpenState = false;
  }

  getGame(gameId) {
    this.gameService.getGame(gameId).subscribe((data: ApiReponse) => {
      this.gameSubject$.next(data.result);
    });
  }

  isAdmin(user: User): boolean {
    return this.authService.isAdmin(user);
  }

  isVoted(game: Game, voteType: string) {
    let isVotedValue = false;
    game.votes.filter((vote: Vote) => vote.voteType === voteType)
      .forEach(data => {
        if (data.userDTO.id === this.authService.currentUserValue.id) {
          isVotedValue = true;
        }
      });
    return isVotedValue;
  }

  voteForGame(voteType: string, gameId: number) {
    const vote = new Vote(voteType, gameId);
    this.voteService.voteForGame(vote).subscribe(() => {
      this.getGame(gameId);
    });
  }

  checkStatistics(gameId: number) {
    this.dialog.open(GameStatisticsDialogComponent, {
      data: {gameId: gameId}
    });
  }

  generateSquads(game: Game) {
    this.dialog.open(GameSquadsDialogComponent, {
      data: {gameId: game.id, plusUsersLength: game.plus}
    });
  }

  isAllowToEditOrDelete(game: Game) {
    const isAdmin = this.authService.isAdmin(this.currentUser);
    const isOwner = game.userDTO.id === this.currentUser.id;
    return isAdmin || isOwner;
  }

  editGame(game: Game) {
    const dialogRef = this.dialog.open(EditGameDialogComponent, {
      data: {game: game}
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.getGame(this.gameId);
      }
    });
  }

  removeGame(game: Game) {
    const dialogRef = this.dialog.open(RemoveGameDialogComponent, {
      data: {game: game}
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.getGame(this.gameId);
      }
    });
  }

  onSubmit(game: Game) {
    this.sendCommentForm.controls['gameDTO'].setValue(game);
    this.gameService.sendComment(this.sendCommentForm.value)
      .subscribe(() => {
        this.getGame(this.gameId);
        this.sendCommentForm.controls['text'].setValue('');
      });
  }

  countComments(game: Game) {
    if (game.comments) {
      if (game.comments.length > 0) {
        if (game.comments.length !== 1) {
          return game.comments.length + ' comments';
        } else {
          return '1 comment';
        }
      } else {
        return 'There are no comments yet';
      }
    } else {
      return 'There are no comments yet';
    }
  }

}
