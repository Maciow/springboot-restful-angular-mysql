package pl.basket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.basket.mapper.Mapper;
import pl.basket.model.*;
import pl.basket.model.DTO.VoteDTO;
import pl.basket.repository.VoteRepository;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collections;
import java.util.List;

@Service
public class VoteService {

    private static final String SUCCESS_VOTE = "Vote has added successfully";

    private VoteRepository voteRepository;
    private Mapper<Vote, VoteDTO> voteMapper;

    @Autowired
    public void setVoteMapper(Mapper<Vote, VoteDTO> voteMapper) {
        this.voteMapper = voteMapper;
    }

    @Autowired
    public void setVoteRepository(VoteRepository voteRepository) {
        this.voteRepository = voteRepository;
    }

    public ApiResponse<VoteDTO> addVote(final Game game, final User user, final VoteType voteType) {
        Vote vote = this.voteRepository.findByGameAndUser(game,user).orElse(null);
        if (vote != null) {
            vote.setVoteType(voteType);
            vote.setLastModified(Timestamp.from(Instant.now()));
            this.voteRepository.save(vote);
            return new ApiResponse<>(200, Collections.singleton(SUCCESS_VOTE), voteMapper.convertToDTO(vote));
        } else {
            Vote newVote = Vote.builder()
                    .game(game)
                    .user(user)
                    .voteType(voteType)
                    .timestamp(Timestamp.from(Instant.now()))
                    .build();
            this.voteRepository.save(newVote);
            return new ApiResponse<>(200, Collections.singleton(SUCCESS_VOTE), voteMapper.convertToDTO(newVote));
        }
    }

    List<Vote> findAllVotesByUser(User user) {
        return this.voteRepository.findAllByUser(user);
    }

    List<Vote> findAllVotesByGameId(Long gameId) {
        return this.voteRepository.findAllByGameId(gameId);
    }
}
