package pl.basket.model.DTO;

import lombok.*;
import pl.basket.model.UserDetails;
import pl.basket.model.UserRole;
import pl.basket.model.UserStatistics;

import java.util.Set;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class UserDTO {

    private Long id;
    private String nickname;
    private String email;
    private Set<UserRole> roles;
    private Boolean isApproved;
    private String imageUrl;
    private UserDetails userDetails;
    private UserStatistics userStatistics;
}
