import {User} from './user';

export class Vote {
  id?: number;
  voteType: string;
  userDTO?: User;
  gameId: number;
  timestamp?: Date;
  lastModified?: Date;

  constructor(voteType: string, gameId: number) {
    this.voteType = voteType;
    this.gameId = gameId;
  }
}
