package pl.basket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.basket.model.Notification;

import java.time.LocalDateTime;

@Repository
@Transactional
public interface NotificationRepository extends JpaRepository<Notification, Long> {

    @Modifying
    @Query(value = "UPDATE notifications SET user_id=:userId WHERE id_notification=:idNotify", nativeQuery = true)
    void setUserToGameNotification(@Param("userId") Long userId, @Param("idNotify") Long idNotify);

    void deleteAllByExpirationDateTimeBefore(LocalDateTime dateTime);
}
