package pl.basket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import pl.basket.mapper.Mapper;
import pl.basket.model.ApiResponse;
import pl.basket.model.DTO.UserDTO;
import pl.basket.model.User;
import pl.basket.model.UserRole;
import pl.basket.repository.AdminRepository;
import pl.basket.repository.UserRoleRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AdminService {

    private final static String NO_SUCH_USER_MESSAGE = "There is no user with such id";
    private final static String USER_UPDATED_SUCCESSFULLY = "%s has updated successfully";
    private final static String DEFAULT_IMG_URL = "https://i.gyazo.com/ae1f5aef3908619b77f7d619352f9d19.png";

    private AdminRepository adminRepository;
    private Mapper<User, UserDTO> userMapper;
    private UserRoleRepository userRoleRepository;

    @Autowired
    public void setUserRoleRepository(UserRoleRepository userRoleRepository) {
        this.userRoleRepository = userRoleRepository;
    }

    @Autowired
    public void setUserMapper(Mapper<User, UserDTO> userMapper) {
        this.userMapper = userMapper;
    }

    @Autowired
    public void setAdminRepository(AdminRepository adminRepository) {
        this.adminRepository = adminRepository;
    }

    public Page<UserDTO> getUsersList(int page, int size) {
        Page<User> users = this.adminRepository.findAll(PageRequest.of(page, size));
        return users.map(user -> this.userMapper.convertToDTO(user));
    }

    public ApiResponse<UserDTO> updateUser(Long userId, UserDTO userDTO) {
        Optional<User> user = this.adminRepository.findById(userId);
        if (user.isPresent()) {
            if (userDTO.getNickname() != null) {
                user.get().setNickname(userDTO.getNickname());
            }
            if (userDTO.getEmail() != null) {
                user.get().setEmail(userDTO.getEmail());
            }
            if (userDTO.getIsApproved() != null) {
                user.get().setIsApproved(userDTO.getIsApproved());
            }
            if (userDTO.getImageUrl() != null) {
                user.get().setImageUrl(userDTO.getImageUrl());
            } else {
                user.get().setImageUrl(DEFAULT_IMG_URL);
            }
            if (!userDTO.getRoles().isEmpty()) {
                user.get().getRoles().clear();
                Set<UserRole> roles = findUserRoles(userDTO.getRoles());
                user.get().getRoles().addAll(roles);
            }
            this.adminRepository.save(user.get());
            return new ApiResponse<>(200, Collections.singleton(String.format(USER_UPDATED_SUCCESSFULLY, user.get().getNickname())), userDTO);
        } else {
            return new ApiResponse<>(400, Collections.singleton(NO_SUCH_USER_MESSAGE), userDTO);
        }
    }

    private Set<UserRole> findUserRoles(Set<UserRole> userRoles) {
        Set<UserRole> roles = new HashSet<>();
        for (UserRole role: userRoles) {
            Optional<UserRole> userRole = this.userRoleRepository.findByRole(role.getRole());
            userRole.ifPresent(roles::add);
        }
        return roles;
    }
}
