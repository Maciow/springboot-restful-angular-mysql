import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveGameDialogComponent } from './remove-game-dialog.component';

describe('RemoveGameDialogComponent', () => {
  let component: RemoveGameDialogComponent;
  let fixture: ComponentFixture<RemoveGameDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemoveGameDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveGameDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
